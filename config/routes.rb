Rails.application.routes.draw do
  get 'home/index'
  get 'trainer_login' => 'advisor#login'
 

  resources :accounts

  devise_for :users
  devise_for :trainees, :skip => [:registrations, :sessions, :passwords]
  devise_for :trainers, :skip => [:registrations, :sessions, :passwords, :logins]	

#scope "/", :defaults => "home#index"

root :to => "home#index"




match "/trainers/sign_in"  => "trainer#signIn", :via => [:get, :post]
match '/trainers/login'  => 'trainer#login', :via => [:get, :post]
match "/trainers/profil", :to => "trainer#profil", :via => [:get, :post]
match "/trainers/trainees", :to => "trainer#afficherTrainees", :via => [:get, :post]
match "/trainers/changepassword", :to => "trainer#changepassword", :via => [:get, :post]
match "/trainers/options", :to => "trainer#options", :via => [:get, :post]
match "/trainers/saveInvoice", :to => "trainer#saveInvoice", :via => [:get, :post]
#match "/trainers/saveProfil", :to => "trainer#options", :via => [:get, :post]



match "/home/trainer"  => "home#trainer", :via => [:get, :post]
match "/home/trainee"  => "home#trainee", :via => [:get, :post]
match "/home/expandableRowTemplate"=> "home#expandableRowTemplate" , :via => [:get, :post]
match "/home/user"  => "home#user", :via => [:get, :post]
match '*any' => 'application#options', :via => [:options]
match "/home/option"  => "home#option", :via => [:get, :post]
match "/home/package"  => "home#package", :via => [:get, :post]
match "/home/savePackage"  => "home#savePackage", :via => [:get, :post]

    match "/trainee/trainee_edit_entry", :to => "trainee#trainee_edit_Entry", :via => [:get, :post]

    match "/trainers/register", :to => "trainer#registration", :via => [:get, :post]
    match "/trainers/sign_in", :to => "trainer#signIn", :via => [:get, :post]
    match "/trainers/sign_out", :to => "trainer#signOut", :via => [:get, :post]
    match "/trainers/template/create", :to => "trainer#create_template", :via => [:get, :post]
    match "/trainers/template/edit", :to => "trainer#edit_template", :via => [:get, :post]
    match "/trainer/delete_templates", :to => "template#delete_template", :via => [:get, :post]

    match "trainer/list_trainees_with_templates", :to => "trainer#list_trainees_with_templates_for_settings", :via => [:get, :post]
    match "trainer/assign_them_templates", :to => "trainer#assign_them_templates", :via => [:get, :post]
    match "trainer/list_selected_trainee_with_templates", :to => "trainer#list_selected_trainee_with_templates", :via => [:get, :post]
    match "trainer/assign_user_templates", :to => "trainer#assign_user_templates", :via => [:get, :post]

    match "/trainer/overall_feedback", :to => "trainer#overall_feedback", :via => [:get, :post]

    match "/trainers/update_loc", :to => "trainer#update_loc", :via => [:get, :post]

    match "/trainers/list_trainees", :to => "trainer#listTrainees", :via => [:get, :post]

    match "/trainee/delete_trainers", :to => "trainee#delete_trainers", :via => [:get, :post]

    match "/trainee/followed_trainers_and_templates_assigned", :to => "trainee#followed_trainers_and_templates_assigned", :via => [:get, :post]

    match "/trainee/set_profile_active", :to => "trainee#set_profile_active", :via => [:get, :post]

    match "/trainee/register", :to => "trainee#registration", :via => [:get, :post]
    match "/trainee/sign_in", :to => "trainee#signIn", :via => [:get, :post]
    match "/trainee/sign_out", :to => "trainee#signOut", :via => [:get, :post]
    match "/trainers/list", :to => "trainee#listAllTrainers", :via => [:get, :post]
    match "/trainers/followed", :to => "trainee#listFollowedTrainers", :via => [:get, :post]

    match "/trainer/home_year", :to => "trainer#home_year", :via => [:get, :post]
    match "/trainer/home_month", :to => "trainer#home_month", :via => [:get, :post]
    match "/trainer/home_day", :to => "trainer#home_day", :via => [:get, :post]
    match "/trainer/list_day_entries", :to => "trainer#list_day_entries", :via => [:get, :post]
    match "/trainer/which_screen_to_jump_to", :to => "trainer#which_screen_to_jump_to", :via => [:get, :post]

    match "/trainer/follow", :to => "trainee#followTrainer", :via => [:get, :post]
    match "/trainee/entry/create", :to => "trainee#create_entry", :via => [:get, :post]
    match "/entry/edit", :to => "trainee#edit_entry", :via => [:get, :post]

    match "/trainee/home_year", :to => "trainee#home_year", :via => [:get, :post]
    match "/trainee/home_month", :to => "trainee#home_month", :via => [:get, :post]
    match "/trainee/home_day", :to => "trainee#home_day", :via => [:get, :post]
    match "/trainee/list_day_entries", :to => "trainee#list_day_entries", :via => [:get, :post]
    match "/trainee/which_screen_to_jump_to", :to => "trainee#which_screen_to_jump_to", :via => [:get, :post]
    match "/trainee/share_entries_with_another_trainer", :to => "trainee#share_entries_with_another_trainer", :via => [:get, :post]
    match "/trainee/trainers_for_a_template", :to => "trainee#trainers_for_a_template", :via => [:get, :post]

    match "/trainee/refresh_blocked_items", :to => "trainee#refresh_blocked_items", :via => [:get, :post]


    match "/trainer/subscribe_to_template", :to => "trainer#subscribe_to_template", :via => [:get, :post]

    match "/template/show_all", :to => "template#show_all", :via => [:get, :post]
    match "/template/show", :to => "template#show", :via => [:get, :post]
    match "/template/show_entry", :to => "template#show_entry", :via => [:get, :post]

    match "/trainee/register_push", :to => "trainee#register_for_push", :via => [:get, :post]
    match "/trainer/register_push", :to => "trainer#register_for_push", :via => [:get, :post]


    match "/trainee/test_notif", :to => "trainee#test_notif", :via => [:get, :post]
    
    match "/trainee/search", :to => "trainee#trainee_search", :via => [:get, :post]
    match "/trainer/search", :to => "trainer#trainer_search", :via => [:get, :post]

    match "/trainer/week_graph_fbk", :to => "trainer#week_graph_fbk", :via => [:get, :post]
    match "/trainer/month_graph_fbk", :to => "trainer#month_graph_fbk", :via => [:get, :post]

    match "/trainee/week_graph_fbk", :to => "trainee#week_graph_fbk", :via => [:get, :post]
    match "/trainee/month_graph_fbk", :to => "trainee#month_graph_fbk", :via => [:get, :post]

    # Forgot Password Routes
    
    match "/trainer/forgot", :to => "resets#trainer_reset_password_token", :via => [:get, :post]
    match "/trainee/forgot", :to => "resets#trainee_reset_password_token", :via => [:get, :post]

    match "/trainer/reset_password", :to => "resets#trainer_change_password", :via => [:get, :post]
    match "/trainee/reset_password", :to => "resets#trainee_change_password", :via => [:get, :post]  

    match "/trainee/delete_entries", :to => "trainee#delete_entries", :via => [:get, :post] 


match "/functions/getallUsers", :to => "home#listAllUsers", :via => [:get, :post] 
match "/functions/getallTrainers", :to => "home#listAllTrainers", :via => [:get, :post] 
match "/functions/getallTrainees", :to => "home#listAllTrainees", :via => [:get, :post]
match "/functions/updateTrainer", :to => "home#updateTrainer", :via => [:get, :post]
match "/functions/getallPackages", :to => "home#listAllPackages", :via => [:get, :post]
match "/functions/getallDiscounts", :to => "home#listAllDiscounts", :via => [:get, :post]
match "/functions/updatePackage", :to => "home#updatePackage", :via => [:get, :post]
match "/functions/updateDiscount", :to => "home#updateDiscount", :via => [:get, :post]
match "/trainers/getallTraineesTrainer", :to => "trainer#listAllTrainees", :via => [:get, :post]

  # end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   #root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
