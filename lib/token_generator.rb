class TokenGenerator
	def self.generate
		while(1)
			token = Devise.friendly_token
			trainer_result=Trainer.where(:token => token).first
			trainee_result=Trainee.where(:token => token).first
			if trainer_result == nil && trainee_result == nil
				return token
			end
		end
	end
end
