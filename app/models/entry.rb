class Entry
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paperclip
  include Mongoid::Search
  # belongs_to :trainee
  belongs_to :template

  after_save :create_analytic
  
  field :entry_fields, type: Hash 

  field :trainee_email, type: String
  field :trainee_name,  type: String

  field :template_name, type: String,         default: ""
  
  field :search_keywords,   type: Array
  field :blocked,           type: Array,      default: [6969]

  field :feedback, 		      type: Boolean,    default: false

  field :file_utc,          type: String,     default: ""

  field :keywords_indexed,	type: Boolean,	  default: false
  # Set to false again after entry has been edited with new btns fak

  has_mongoid_attached_file :file, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment_content_type :file, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  # do_not_validate_attachment_file_type :image

  search_in :trainee_name#, :entry_fields => [:responses]

  private

  def create_analytic

  	btns = []
  	
  	srch_btns = []

  	self.entry_fields["responses"].each do |er|
  		if er["active"] == "1"
  			btns << er["title"]
  		end

      er["subs"].each do |sub|
        if sub["type"] == "Text Field"
          if sub["response"]
            srch_btns << sub["response"].downcase
          end
        elsif sub["type"] == "Dropdown"
          if sub["response"] != "6969"
            srch_btns << sub["options"][sub["response"].to_i].downcase
          end
        end
        srch_btns << sub["title"].downcase
      end
  		
  		srch_btns << er["title"].downcase
  	end

    srch_btns << self.template_name.downcase

    if self.template
      srch_btns << "#{self.template.trainer.f_name.downcase} #{self.template.trainer.l_name.downcase}"
    end

    if self.template
      btns.each do |b|
        Analytic.create_daily_records(b, self.template.id.to_s, self.trainee_email, self.created_at)
        Analytic.create_monthly_records(b, self.template.id.to_s, self.trainee_email, self.created_at)
        Analytic.create_yearly_records(b, self.template.id.to_s, self.trainee_email, self.created_at)
        Analytic.create_alltime_records(b, self.template.id.to_s, self.trainee_email, self.created_at)
      end
    end
  	
    # Search keywords being added
  	if self.keywords_indexed == false
      self.update_attributes!(:search_keywords => srch_btns, :keywords_indexed => true)
      if self.blocked == nil
        self.update_attributes!(:blocked => [6969])
      end
	  end

    if self.template
      Alltime.create_alltime_record(self.template.id.to_s, self.trainee_email)      
    end

  end

  def self.send_trainer_notif trainee, entry, new_entry

    pusher = Grocer.pusher(
      certificate: "/home/osman/certs/bluewaveProdCert.pem",
      passphrase:  "bluewave123@",                       
      gateway:     "gateway.push.apple.com",
    )

    tokenz = []
    trainer = entry.template.trainer

    trainer.trainerdevices.each do |de|
      tokenz << de.push_token
    end



    al = ""

    if new_entry == "true"
      al = "#{trainee.f_name} has just submitted a new #{entry.template_name} entry"
    elsif new_entry == "false"
      al = "#{trainee.f_name} has just edited a #{entry.template_name} entry"
    end

    Rails.logger.info "This was the new entry #{new_entry} and al was #{al}"

    tokenz.each do |t|
      notification = Grocer::Notification.new(
        device_token:      t,
        alert:             al,
        custom:            {:entry_id => "trainer-_-#{entry.id.to_s}", :created_at => entry.created_at, :trainee => entry.trainee_email}.as_json,
        expiry:            Time.now + 60*60
      )

      pusher.push(notification)
    end

  end

  def self.send_trainee_notif trainer, entry

    pusher = Grocer.pusher(
      certificate: "/var/www/rails1/shared/certs/pushcert.pem",
      passphrase:  "",                       
      gateway:     "gateway.push.apple.com",
    )

    tokenz = []
    trainee = Trainee.where(:email => entry.trainee_email).first

    trainee.traineedevices.each do |de|
      tokenz << de.push_token
    end

    tokenz.each do |t|
      notification = Grocer::Notification.new(
        device_token:      t,
        alert:             "You have just received feedback from your #{entry.template_name} advisor, #{trainer.f_name}",
        custom:            {:entry_id => "trainee-_-#{entry.id.to_s}", :created_at => entry.created_at}.as_json,
        expiry:            Time.now + 60*60
      )

      pusher.push(notification)
    end

  end

end