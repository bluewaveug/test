class Alltime
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :trainee

  field :trainer_id, 	type: String

  field :red,			type: Float
  field :yellow,		type: Float
  field :green,			type: Float

  field :fbk,			type: Float

  private

  def self.create_alltime_record (template_id, trainee_email)

  	trainee  =  Trainee.where(:email => trainee_email).first
  	template =  Template.find(template_id)
  	
  	trainerz =  []
  	trainerz << template.trainer.id.to_s

  	shared   =  trainee.shares.where(:template_id => template_id)

  	shared.each do |sh|
  		trainerz << sh.trainer_id
  	end

  	trainerz.each do |t|
  		trainer    =  Trainer.find(t)
  		templates  =  trainer.templates.distinct(:id)

  		alltime    =  trainee.alltimes.where(:trainer_id => trainer.id.to_s).first

  		all_entries  =  Entry.where(:trainee_email => trainee_email).any_in(:template_id => templates)

  		if all_entries.count != 0

  			fbk = 0

			red    = 0.0
			green  = 0.0
			yellow = 0.0

			all_entries.each do |e|
				e.entry_fields["responses"].each do |er|
					if er["active"] == "1"
            er["subs"].each do |sub|
              if sub["active"] == "1"
                if sub["feedback"] == "1"
                  red += 1
                elsif sub["feedback"] == "2"
                  yellow += 1
                elsif sub["feedback"] == "3"
                  green += 1
                end
              end
            end
					end
				end
			end

			if (red + yellow + green) != 0
				avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
			
				fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
			end

			if alltime
				alltime.update_attributes!(:red => red, :yellow => yellow, :green => green, :fbk => fbk, :trainer_id => t.to_s)
			else
				trainee.alltimes.create!(:red => red, :yellow => yellow, :green => green, :fbk => fbk, :trainer_id => t.to_s)
			end
  		end 
  	end
  end

end
