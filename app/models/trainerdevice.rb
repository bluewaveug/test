class Trainerdevice
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :trainer

  field :push_token, type: String
end
