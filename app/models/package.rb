class Package
  include Mongoid::Document
  include Mongoid::Timestamps
  has_many :subscriptions

  field :title,        type: String
  field :max_users, type: Integer
  field :price, type: Float
  field :type, type: Integer
  field :statut, type: Boolean, default: true
end
