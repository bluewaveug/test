class Invoice
  include Mongoid::Document
  include Mongoid::Timestamps
  
  belongs_to :trainer


  field :price,        	type: Float, default: 0
  field :title,        type: String 

  field :date_end, 	type: Time
  field :date_begin, 	type: Time

  field :next, 		type: Boolean, default: false

  field :discountPackageUp,   type: Float, default: 0
  field :discountPrePaid, type: Float, default: 0
  
  field :total, 	type: Float
  
  field :maxUsers, type: Integer
  

end
