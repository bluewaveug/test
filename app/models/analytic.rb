class Analytic
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :trainee

  field :title,			type: String
  field :template_id,	type: String

  field :time_type,		type: String
  field :day,			type: String
  field :month,			type: String
  field :year,			type: String
  
  field :red,			type: Float
  field :yellow,		type: Float
  field :green,			type: Float

  field :fbk,			type: Float

  private

  def self.create_daily_records    (title, template_id, trainee_email, date_created)
  		current_timezone = Time.zone
		Time.zone = "UTC"

		day    =  date_created.day
		month  =  date_created.month
		year   =  date_created.year

		trainee  =  Trainee.where(:email => trainee_email).first
		template =  Template.find(template_id)

		if trainee
			anal     =  trainee.analytics.where(:title => title, :day => day, :month => month, :year => year, :time_type => "day").first

			starting =  Time.zone.parse("#{year}-#{month}-#{day}")
			ending   =  Time.zone.parse("#{year}-#{month}-#{day}") + 1.day

			all_entries = Entry.where(:trainee_email => trainee_email)

			entries = all_entries.where(:created_at.gt => starting, :created_at.lt => ending)

			fbk_entries = entries.where(:feedback => true)

			if fbk_entries.count != 0

				fbk = 0

				red    = 0.0
				green  = 0.0
				yellow = 0.0
				
				fbk_entries.each do |e|
					e.entry_fields["responses"].each do |er|
						if er["active"] == "1" && er["title"] == title
							er["subs"].each do |sub|
								if sub["active"] == "1"
									if sub["feedback"] == "1"
										red += 1
									elsif sub["feedback"] == "2"
										yellow += 1
									elsif sub["feedback"] == "3"
										green += 1
									end
								end
							end
						end
					end
				end

				if (red + yellow + green) != 0
					avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
				
					fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
				end
				
				if anal
					anal.update_attributes!(:red => red, :yellow => yellow, :green => green, :fbk => fbk)
				else
					trainee.analytics.create!(:title => title, :time_type => "day", :day => day, :month => month, :year => year,
					 :red => red, :yellow => yellow, :green => green, :fbk => fbk)
				end

			end
		end

		Time.zone = current_timezone
  end

  def self.create_monthly_records  (title, template_id, trainee_email, date_created)

  		current_timezone = Time.zone
		Time.zone = "UTC"

		month  =  date_created.month
		year   =  date_created.year


		trainee  =  Trainee.where(:email => trainee_email).first
		template =  Template.find(template_id)

		if trainee
			anal     =  trainee.analytics.where(:title => title, :month => month, :year => year, :time_type => "month").first

			day_count = Time.days_in_month(month.to_i, year.to_i)

			starting =  Time.zone.parse("#{year}-#{month}-01")
			ending   =  Time.zone.parse("#{year}-#{month}-#{day_count}") + 1.day

			all_entries = Entry.where(:trainee_email => trainee_email)

			entries = all_entries.where(:created_at.gt => starting, :created_at.lt => ending)

			fbk_entries = entries.where(:feedback => true)

			if fbk_entries.count != 0

				fbk = 0

				red    = 0.0
				green  = 0.0
				yellow = 0.0
				
				fbk_entries.each do |e|
					e.entry_fields["responses"].each do |er|
						if er["active"] == "1" && er["title"] == title 
							er["subs"].each do |sub|
								if sub["active"] == "1"
									if sub["feedback"] == "1"
										red += 1
									elsif sub["feedback"] == "2"
										yellow += 1
									elsif sub["feedback"] == "3"
										green += 1
									end
								end
							end
						end
					end
				end

				if (red + yellow + green) != 0
					avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)

					fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
				end

				if anal
					anal.update_attributes!(:red => red, :yellow => yellow, :green => green, :fbk => fbk)
				else
					trainee.analytics.create!(:title => title, :time_type => "month", :month => month, :year => year,
					 :red => red, :yellow => yellow, :green => green, :fbk => fbk)
				end

			end
		end

		Time.zone = current_timezone
  end

  def self.create_yearly_records   (title, template_id, trainee_email, date_created)
  		current_timezone = Time.zone
		Time.zone = "UTC"

		year   =  date_created.year

		trainee  =  Trainee.where(:email => trainee_email).first
		template =  Template.find(template_id)

		if trainee
			anal     =  trainee.analytics.where(:title => title, :year => year, :time_type => "year").first


			starting =  Time.zone.parse("#{year}-01-01")
			ending   =  Time.zone.parse("#{year}-12-31") + 1.day

			all_entries = Entry.where(:trainee_email => trainee_email)

			entries = all_entries.where(:created_at.gt => starting, :created_at.lt => ending)

			fbk_entries = entries.where(:feedback => true)

			if fbk_entries.count != 0

				fbk = 0

				red    = 0.0
				green  = 0.0
				yellow = 0.0
				
				fbk_entries.each do |e|
					e.entry_fields["responses"].each do |er|
						if er["active"] == "1" && er["title"] == title 
							er["subs"].each do |sub|
								if sub["active"] == "1"
									if sub["feedback"] == "1"
										red += 1
									elsif sub["feedback"] == "2"
										yellow += 1
									elsif sub["feedback"] == "3"
										green += 1
									end
								end
							end
						end
					end
				end

				if (red + yellow + green) != 0
					avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)

					fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
				end

				if anal
					anal.update_attributes!(:red => red, :yellow => yellow, :green => green, :fbk => fbk)
				else
					trainee.analytics.create!(:title => title, :time_type => "year", :year => year,
					 :red => red, :yellow => yellow, :green => green, :fbk => fbk)
				end

			end
		end

		Time.zone = current_timezone
  end

  def self.create_alltime_records   (title, template_id, trainee_email, date_created)

  		current_timezone = Time.zone
		Time.zone = "UTC"

		#year   =  date_created.year

		trainee  =  Trainee.where(:email => trainee_email).first
		template =  Template.find(template_id)

		if trainee
			anal     =  trainee.analytics.where(:title => title, :time_type => "alltime").first


			#starting =  Time.zone.parse("#{year}-01-01")
			#ending   =  Time.zone.parse("#{year}-12-31") + 1.day

			all_entries = Entry.where(:trainee_email => trainee_email)

			#entries = all_entries.where(:created_at.gt => starting, :created_at.lt => ending)

			fbk_entries = all_entries.where(:feedback => true)

			if fbk_entries.count != 0

				fbk = 0

				red    = 0.0
				green  = 0.0
				yellow = 0.0
				
				fbk_entries.each do |e|
					e.entry_fields["responses"].each do |er|
						if er["active"] == "1" && er["title"] == title
							er["subs"].each do |sub|
								if sub["active"] == "1"
									if sub["feedback"] == "1"
										red += 1
									elsif sub["feedback"] == "2"
										yellow += 1
									elsif sub["feedback"] == "3"
										green += 1
									end
								end
							end
						end
					end
				end

				if (red + yellow + green) != 0
					avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)

					fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
				end

				if anal
					anal.update_attributes!(:red => red, :yellow => yellow, :green => green, :fbk => fbk)
				else
					trainee.analytics.create!(:title => title, :time_type => "alltime",
					 :red => red, :yellow => yellow, :green => green, :fbk => fbk)
				end

			end
		end

		Time.zone = current_timezone

  end

end
