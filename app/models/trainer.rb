class Trainer
  include Mongoid::Document
  include Mongoid::Timestamps
  has_and_belongs_to_many :trainees
  has_many :templates
  has_many :trainerdevices

  has_many :subscriptions
  has_many :invoices

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :f_name,             type: String, default: ""
  field :l_name,             type: String, default: ""
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :token, type: String, default: ""
  field :isActive, 	     type: Boolean, default: true
  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time
  field :loc, type: Array,  default: [0,0]
  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String


  field :package,    type: String

  ## Indexing
  index({ loc: "2d" }, { min: -200, max: 200 })

  

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  private

  def self.get_geo_trainers trainee, geo, radius
    trainers = []

    result = Trainer.where(:loc => {"$near" => geo , '$maxDistance' => radius.fdiv(69)})

    result.each do |t|
      trainers << {:email => t.email, :loc => t.loc, :fname => t.f_name, :lname => t.l_name, :followed => trainee.trainers.include?(t)}
    end

    return trainers
  end

  def self.get_trainers     trainee
    trainers = []

    Trainer.all.each do |t|
      trainers << {:email => t.email, :loc => t.loc, :fname => t.f_name, :lname => t.l_name, :followed => trainee.trainers.include?(t)}
    end

    return trainers
  end

end
