class Template
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :trainer
  has_many :entries

  field :name
  field :template_fields,         type: Hash,  default: {}
  field :descItem,                type: Hash
  field :number_of_entries,       type: Integer, default: 0
  field :sharing_trainers_email,  type: Array, default: []

  field :frequency,			          type: String
  field :frequency_type,	        type: String

end