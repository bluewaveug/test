class Feedback
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :trainee

  field :type,		    type: String,	default: ""

  field :day,			type: String,	default: ""
  field :month,			type: String,	default: ""
  field :year,			type: String,	default: ""

  field :text,			type: String,	default: ""

  field :trainer,       type: String,	default: ""

end
