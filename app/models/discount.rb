class Discount
  include Mongoid::Document
  include Mongoid::Timestamps
  
  has_many :subscriptions

  field :title,      type: String
  field :percentage, type: Float
  field :type, 	     type: Integer

end
