class Traineedevice
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :trainee

  field :push_token, type: String
end
