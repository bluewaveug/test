class Subscription
  include Mongoid::Document
  include Mongoid::Timestamps
  
  belongs_to :package
  belongs_to :trainer

  devise :database_authenticatable, :registerable,
        :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :discountPrePaid,   type: Float
  field :discountPackageUp, type: Float
end
