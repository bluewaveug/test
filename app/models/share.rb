class Share
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :trainee

  field :template_id,	type: String
  field :trainer_id,	type: String
end
