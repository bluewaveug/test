class TraineeController < ApplicationController
	require "#{Rails.root}/lib/token_generator"
	before_filter :trainee_token_authentication, :except => [:registration, :signIn, :edit_entry, :test_notif]
	before_filter :trainer_token_authentication, :only => [:edit_entry]
	skip_before_action :verify_authenticity_token
	#rescue_from Exception, :with => :throw_error
	#rescue_from Mongoid::Errors::Validations, :with => :throw_error
		
	def registration
		if Trainee.where(:email => params["trainee"]["email"]).first == nil
			trainee=Trainee.create trainee_params
			token=TokenGenerator.generate
			trainee.update_attributes :token => token
			render :json => {:message => "Successfully created", :trainee_id => trainee.id.to_s, :token => token}, :status => 201
		else
			render :json => {:message => "Email alerady exists"}, :status => 401
		end
	end

	def signIn
		trainee=Trainee.where(:email => params["trainee"]["email"]).first
		if trainee
			if trainee.valid_password?(params["trainee"]["password"])
				token=TokenGenerator.generate
				trainee.update_attributes :token => token

				render :json => {:message => "Successfully login", :trainer_id => trainee.id.to_s, :token => token}
			else
				render :json => {:message => "Wrong creds"}, :status => 401
			end
		else
			render :json => {:message => "User not found"}, :status => 404
		end	
	end

	def share_entries_with_another_trainer

		user_not_found 	   		if current_trainee == nil

		template = Template.find(params["template_id"])
		template_not_found 		if template == nil

		current_trainee.shares.delete_all

		flag = true

		erronous_email = ""

		params[:trainer].each do |t|
			trainer  = Trainer.where(:email => t).first

			if trainer
				if template.trainer != trainer
					new_share = current_trainee.shares.create!(:template_id => params["template_id"], :trainer_id => 
						trainer.id.to_s)
				end
			else
				flag = false
				erronous_email = t
			end
		end

		if flag
			render :json => {:message => "Successfully shared"}, :status => 200
		else
			render :json => {:message => "Trainer not found", :email => erronous_email}, :status => 404
		end
	end

	def trainers_for_a_template
		user_not_found 	   		if current_trainee == nil

		template = Template.find(params["template_id"])
		template_not_found 		if template == nil

		trainerz = []

		current_trainee.trainers.each do |t|

			sh = current_trainee.shares.where(:template_id => template.id, :trainer_id => t.id).first

			if template.trainer == t
				trainerz << {:trainer => t.as_json, :shared => true}
			elsif sh
				trainerz << {:trainer => t.as_json, :shared => true}
			else
				trainerz << {:trainer => t.as_json, :shared => false}
			end
		end

		render :json => trainerz
	end

	def which_screen_to_jump_to

		if current_trainee
			total = Entry.where(:trainee_email => current_trainee.email).asc(:created_at)

				screen = "none"
				
				year  = "none"
				month = "none"
				day   = "none"

				if total.count != 0

					first = total.first.created_at#.to_time.to_i

					last  = total.last.created_at#.to_time.to_i

					#diff = (last - first) / 86400

					if last.year == first.year
						if last.month == first.month
							if last.day == first.day
								# Only one day
								screen = "day"
								year   = first.year
								month  = first.month
								day    = first.day
							else	
								# Only one day's worth of entries
								screen = "days"
								year   = first.year
								month  = first.month
							end
						else
							# More tahn one month
							screen = "months"
							year   = first.year
						end
					else
						# More than one year
						screen = "years"
					end
						
				end

				render :json => {:screen => screen, :year => year, :month => month, :day => day}, :status => 200
		else
			render :json => {:message => "User not found"}, :status => 404
		end

	end

	def signOut
		current_trainee.update_attributes :token => nil
		render :json => {:message => "log out log out"}
	end

	def listAllTrainers
		if current_trainee

			trainers = []

			if params["trainee"]["geo"] && params["trainee"]["radius"]
				trainers = Trainer.get_geo_trainers current_trainee, params["trainee"]["geo"].map(&:to_f), params["trainee"]["radius"].to_f
			else
				trainers = Trainer.get_trainers current_trainee
			end
			
			render :json => trainers
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end
	
	def trainee_search
		if current_trainee

			if params["starting"] == "history" && params["ending"] == "history"
				all_entries = Entry.where(:trainee_email => current_trainee.email)
			else
				current_timezone = Time.zone
				Time.zone = "UTC"

				starting = Time.zone.parse(params["starting"])
				ending   = Time.zone.parse(params["ending"])

				all_entries = Entry.where(:trainee_email => current_trainee.email, :created_at.gt => starting, :created_at.lt => ending)

				Time.zone = current_timezone
			end
			
			result = all_entries.any_of(:search_keywords => /#{params["query"].downcase}/).desc(:created_at).limit(10).offset(params["offset"].to_i)
			
			result_query = []
			
			result.each do |re|
				
				fbk = 0

				if re.feedback
					
					red = 0.0
					green = 0.0
					yellow = 0.0

					re.entry_fields["responses"].each do |er|
						if er["active"] == "1"
							er["subs"].each do |sub|
								if sub["active"] == "1"
									if sub["feedback"] == "1"
										red += 1
									elsif sub["feedback"] == "2"
										yellow += 1
									elsif sub["feedback"] == "3"
										green += 1
									end
								end
							end
						end
					end

					if (red + yellow + green) != 0
						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)

						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
					end
				end
				
				result_query << {:entry => re.as_json.except("file_file_name", "file_content_type", "file_file_size", "file_updated_at",
					"file_fingerprint"), :file => {:med => re.file.url(:medium), :org => re.file.url}, 
					:template => re.template_name, :feedback => fbk, }
					
			end
			
			render :json => {:result => result_query}, :status => 200
		else
			render :json => {:message => "User not found"}, :status => 404
		end
		
	end

	def listFollowedTrainers
		if current_trainee

			trainers = []

			current_trainee.trainers.each do |t|
				trainers << t
			end
			
			render :json => trainers
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def followed_trainers_and_templates_assigned

		trainerz = current_trainee.trainers.as_json

		all_assigned = current_trainee.templates_array

		trainerz.each do |t|
			rt = Trainer.find(t["_id"].to_s)
			trainer_all = rt.templates.distinct(:id).map &->(x){ x.to_s }
			trainer_assigned = all_assigned & trainer_all
			ates = []
			trainer_assigned.each do |ta|
				tax = Template.find(ta).as_json
				if tax
					if current_trainee.hidden_templates.index(ta)
						tax.merge!(:active => "0")
					else
						tax.merge!(:active => "1")
					end
					ates << tax
				end
			end
			t.merge!(:trainer_templates => ates)
		end

		render :json => {:trainerz => trainerz}, :status => 200
	end

	def set_profile_active

		hidden = []

		params["trainerz"].each do |key, tr|
			if tr != "6969"
				tr.each do |temp_id, active|
					if active == "0"
						hidden << temp_id
					end
				end
			end
		end

		# params["trainerz"].each do |tr|
		# 	if tr["trainer_templates"]
		# 		tr["trainer_templates"].each do |tt|
		# 			if tt["active"] == "0"
		# 				hidden << tt["_id"]["$oid"]
		# 			end
		# 		end
		# 	end
		# end

		current_trainee.update_attributes!(:hidden_templates => hidden)

		render :json => {:message => "Success"}, :status => 200
	end

	def create_entry
		if current_trainee

			template = Template.where(:_id => params["template"]["template_id"]).first

			t_frequency 		= template.frequency
			t_frequency_type 	= template.frequency_type

			e_timestamp 		= Time.zone.parse(entry_params["timestamp"])

			cnt = 0

			realBlockedItems = []

			entry_params["entry_fields"]["responses"].count.times do |i|
				if params["blockedItems"]
					if params["blockedItems"]["#{i}"]
						realBlockedItems << params["blockedItems"]["#{i}"]
					else
						realBlockedItems << []
					end
				else
					realBlockedItems << []
				end
			end

			Rails.logger.info " t_frequency_type is #{t_frequency_type}"

			if t_frequency_type == "Always" || t_frequency_type == "Unlimited"

				Rails.logger.info "Here"

				new_entry = template.entries.create!(:created_at => Time.zone.parse(entry_params["timestamp"]),
				 :entry_fields => entry_params["entry_fields"], :trainee_email => current_trainee.email,
				  :trainee_name => "#{current_trainee.f_name} #{current_trainee.l_name}", :template_name => template.name,
				   :blocked => realBlockedItems)

				if params[:entry] && params[:entry][:file]

					decoded_data = Base64.decode64(params[:entry][:file])

					data = StringIO.new(decoded_data)

					new_entry.file  = data

					new_entry.file_utc = Time.now.utc.to_i.to_s

					new_entry.save!

				end
				Entry.send_trainer_notif current_trainee, new_entry, "true"

				render :json => {:message => "Created"}, :status => 201
			else
				if    t_frequency_type == "Day"
					cnt = template.entries.where(:trainee_email => current_trainee.email, :created_at.gt =>
					 e_timestamp.beginning_of_day, :created_at.lt => e_timestamp.end_of_day).count
				elsif t_frequency_type == "Week"
					cnt = template.entries.where(:trainee_email => current_trainee.email, :created_at.gt =>
					 e_timestamp.beginning_of_week, :created_at.lt => e_timestamp.end_of_week).count					
				elsif t_frequency_type == "Month"
					cnt = template.entries.where(:trainee_email => current_trainee.email, :created_at.gt =>
					 e_timestamp.beginning_of_month, :created_at.lt => e_timestamp.end_of_month).count										
				end

				if cnt < t_frequency.to_i
					new_entry = template.entries.create!(:created_at => Time.zone.parse(entry_params["timestamp"]),
					 :entry_fields => entry_params["entry_fields"], :trainee_email => current_trainee.email,
					  :trainee_name => "#{current_trainee.f_name} #{current_trainee.l_name}", :template_name => template.name,
					   :blocked => realBlockedItems)

					if params[:entry] && params[:entry][:file]

						decoded_data = Base64.decode64(params[:entry][:file])

						data = StringIO.new(decoded_data)

						new_entry.file  = data

						new_entry.file_utc = Time.now.utc.to_i.to_s

						new_entry.save!

					end
					Entry.send_trainer_notif current_trainee, new_entry, "true"

					render :json => {:message => "Created"}, :status => 201
				
				else
					if    t_frequency_type == "Day"
						render :json => {:message => "You cannot create any more entries for this profile this day"},
						 :status => 404
					elsif t_frequency_type == "Week"
						render :json => {:message => "You cannot create any more entries for this profile this week"},
						 :status => 404
					elsif t_frequency_type == "Month"
						render :json => {:message => "You cannot create any more entries for this profile this month"},
						 :status => 404
					end
				end

			end

		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def edit_entry
		if current_trainer
			entry = Entry.where(:_id => params["entry"]["entry_id"]).first

			existing_search_keywords = entry.search_keywords

			if entry_params["entry_fields"]["trainer_notes"]
				existing_search_keywords << entry_params["entry_fields"]["trainer_notes"].downcase
			end

			entry.update_attributes!(:entry_fields => entry_params["entry_fields"], :feedback => true, :search_keywords => 
				existing_search_keywords)

			Entry.send_trainee_notif current_trainer, entry

			render :json => {:message => "Updated"}, :status => 201
		end
	end

	def trainee_edit_Entry
		if current_trainee
			entry = Entry.where(:_id => params["entry"]["entry_id"]).first

			entry.update_attributes!(:entry_fields => entry_params["entry_fields"])

			if params[:entry] && params[:entry][:file]

				decoded_data = Base64.decode64(params[:entry][:file])

				data = StringIO.new(decoded_data)

				entry.file  = data

				entry.file_utc = Time.now.utc.to_i.to_s

				entry.save!
			end

			Entry.send_trainer_notif current_trainee, entry, "false"

			#Entry.send_trainee_notif current_trainer, entry

			render :json => {:message => "Updated"}, :status => 201
		end
	end

	def delete_entries
		if current_trainee

			if params[:idz]
				params[:idz].each do |id|
					e = Entry.find(id)

					if e
						e.delete
					end
				end	
			end

			render :json => {:message => "Entries deleted"}, :status => 201
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def refresh_blocked_items
		temp  =  Template.find(params["template"]["template_id"])

		blocked = []

		e_timestamp = Time.zone.parse(params["timestamp"])

		temp.template_fields["items"].each_with_index do |i, index|

			blocked[index] = []
					
			i["subs"].each_with_index do |l, sub_index|
				if l["variable_frequency"] != "Always" && l["variable_frequency"] != "Unlimited"

					t_frequency 		= l["variable_frequency"].split(" x ")[0]
					t_frequency_type 	= l["variable_frequency"].split(" x ")[1]

					cnt = 0

					if    t_frequency_type == "Day"
						all_entries = temp.entries.where(:trainee_email => current_trainee.email, :created_at.gt =>
						 e_timestamp.beginning_of_day, :created_at.lt => e_timestamp.end_of_day)
					elsif t_frequency_type == "Week"
						all_entries = temp.entries.where(:trainee_email => current_trainee.email, :created_at.gt =>
						 e_timestamp.beginning_of_week, :created_at.lt => e_timestamp.end_of_week)					
					elsif t_frequency_type == "Month"
						all_entries = temp.entries.where(:trainee_email => current_trainee.email, :created_at.gt =>
						 e_timestamp.beginning_of_month, :created_at.lt => e_timestamp.end_of_month)										
					end

					all_entries.each do |e|
						e.entry_fields["responses"].each do |r|
							r["subs"].each do |s|
								if s["title"] == l["title"] && s["response"] != nil && s["response"] != ""
									cnt += 1
								end
							end
						end
					end

					if cnt >= t_frequency.to_i
						blocked[index] << sub_index  
					end
				end
			end
		end

		render :json => {:blocked => blocked}, :status => 200
	end

	def register_for_push
		if current_trainee

			real_token = register_push_params["push_token"]

			real_token.slice!("<")

			real_token.slice!(">")

			real_token.gsub!(" ", "")

			existing_devices = Traineedevice.where(:push_token => real_token)

			if existing_devices.count != 0
				existing_devices.delete_all
			end

			current_trainee.traineedevices.delete_all

			if current_trainee.traineedevices.create!(:push_token => real_token)
				render :json => {:message => "Added device for trainee"}, :status => 200
			else
				render :json => {:message => "OOps"}, :status => 500
			end
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def week_graph_fbk

		current_timezone = Time.zone
		Time.zone = "UTC"

		exc = []

		if params["except"]
			exc = params["except"]
		end

		btns = {}

		all_entries = Entry.where(:trainee_email => params["trainee"]["email"], :created_at.gt => Time.zone.parse(params["starting"]), 
			:created_at.lt => Time.zone.parse(params["ending"]) + 1.day).not_in(:template_id => current_trainee.hidden_templates)

		fbk_entries = all_entries.where(:feedback => true)

		starting = Date.parse(params["starting"])
		ending   = Date.parse(params["ending"])

		range_array = (starting..ending).map{|d| [d.day, d.month, d.year]}.uniq

		if fbk_entries.count != 0
			
			fbk_entries.each do |e|
				e.entry_fields["responses"].each do |er|

					if er["active"] == "1"
						
						if er["title"].in?(exc) == false
							if !btns.has_key?(er["title"])

								red = 0.0
								green = 0.0
								yellow = 0.0

								fbk = 0.0

								range_array.each do |rd|
									anal = current_trainee.analytics.where(:time_type => "day", :day => rd[0], :month => rd[1], :year => rd[2], :title => er["title"]).first

									if anal
										red += anal.red
										green += anal.green
										yellow += anal.yellow
									end
								end

								if (red + yellow + green) != 0

									avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
								
									fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
								end
								
								btns["#{er["title"]}"] = fbk
							end
						else
							if !btns.has_key?(er["title"])
								btns["#{er["title"]}"] = -1
							end
						end
					end
				end
			end
		end

		Time.zone = current_timezone

		final = {:btns => btns}

		render :json => final

	end

	def month_graph_fbk

		current_timezone = Time.zone
		Time.zone = "UTC"

		exc = []

		if params["except"]
			exc = params["except"]
		end

		btns = {}

		all_entries = Entry.where(:trainee_email => params["trainee"]["email"], :created_at.gt => Time.zone.parse(params["starting"]), 
			:created_at.lt => Time.zone.parse(params["ending"]) + 1.day).not_in(:template_id => current_trainee.hidden_templates)

		fbk_entries = all_entries.where(:feedback => true)

		starting = Date.parse(params["starting"])
		ending   = Date.parse(params["ending"])

		range_array = (starting..ending).map{|d| [d.month, d.year]}.uniq

		if fbk_entries.count != 0
			
			fbk_entries.each do |e|
				e.entry_fields["responses"].each do |er|

					if er["active"] == "1"
						
						if er["title"].in?(exc) == false
							if !btns.has_key?(er["title"])

								red = 0.0
								green = 0.0
								yellow = 0.0

								fbk = 0.0

								range_array.each do |rd|
									anal = current_trainee.analytics.where(:time_type => "month", :month => rd[0], :year => rd[1], :title => er["title"]).first

									if anal
										red += anal.red
										green += anal.green
										yellow += anal.yellow
									end
								end

								if (red + yellow + green) != 0

									avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
								
									fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
								end
								
								btns["#{er["title"]}"] = fbk
							end
						else
							if !btns.has_key?(er["title"])
								btns["#{er["title"]}"] = -1
							end
						end
					end
				end
			end
		end

		Time.zone = current_timezone

		final = {:btns => btns}

		render :json => final

	end

	# def home_year

	# 	user_not_found	if current_trainee == nil

	# 	exc   = []

	# 	exc   = params["except"] if params["except"]

	# 	years = current_trainee.analytics.distinct(:year)

	# 	years.each do |y|

	# 	end


	# end

	def home_year
		if current_trainee

			current_timezone = Time.zone
			Time.zone = "UTC"

			exc = []

			if params["except"]
				exc = params["except"]
			end

			result = []
			btns = {}
			graph_btns = {}
			excluded_buttons = {}
			big_graph = {}

			big_graph["big_graph_datapoints"] = []

			feedbacks_test = [0,1,2,3]

			all_entries = Entry.where(:trainee_email => current_trainee.email).not_in(:template_id => current_trainee.hidden_templates)

			all_dates   = all_entries.distinct(:created_at)

			years = []

			all_dates.each do |d|
				years << d.year
			end

			years.uniq!

			years.count.times do |xtx|
				big_graph["big_graph_datapoints"] << 0
			end

			real_big_fbk = 0
			real_big_colors = [0.0, 0.0, 0.0]

			years.each do |y|

				fbk = 0

				big_fbk = 0

				starting = Time.zone.parse("#{y}-01-01")
				ending   = Time.zone.parse("#{y}-12-31") + 1.day

				year_entries = all_entries.where(:created_at.gt => starting, :created_at.lt => ending)

				fbk_entries  = year_entries.where(:feedback => true)

				if fbk_entries.count != 0

					red = 0.0
					green = 0.0
					yellow = 0.0

					big_red = 0.0
					big_green = 0.0
					big_yellow = 0.0

					fbk_entries.each do |e|
						e.entry_fields["responses"].each do |er|

							if er["active"] == "1"

								er["subs"].each do |sub|
									if sub["active"] == "1"
										if sub["feedback"] == "1"
											big_red += 1
											real_big_colors[0] += 1
										elsif sub["feedback"] == "2"
											big_yellow += 1
											real_big_colors[1] += 1
										elsif sub["feedback"] == "3"
											big_green += 1
											real_big_colors[2] += 1
										end
									end
								end
								
								if er["title"].in?(exc) == false
									if !btns.has_key?(er["title"])
										anal = current_trainee.analytics.where(:time_type => "alltime",
										 :title => er["title"]).first

										if anal
											btns["#{er["title"]}"] = anal.fbk
										end
									end

									er["subs"].each do |sub|
										if sub["active"] == "1"
											if sub["feedback"] == "1"
												red += 1
											elsif sub["feedback"] == "2"
												yellow += 1
											elsif sub["feedback"] == "3"
												green += 1
											end
										end
									end
								else

									if !btns.has_key?(er["title"])
										btns["#{er["title"]}"] = -1
									end

									if !excluded_buttons.has_key?(er["title"])
										exc_anal = current_trainee.analytics.where(:time_type => "alltime",
										 :title => er["title"]).first

										if exc_anal
											excluded_buttons["#{er["title"]}"] = exc_anal.fbk
										end
									end

								end

								current_index = years.index(y)

								if graph_btns.has_key?("#{er["title"]}")

									if graph_btns["#{er["title"]}"][current_index] == 0

										graph_anal = current_trainee.analytics.where(:time_type => "year", 
											:year => y, :title => er["title"]).first

										if graph_anal
											graph_btns["#{er["title"]}"][current_index] = graph_anal.fbk
										else
											graph_btns["#{er["title"]}"][current_index] = 0
										end
									end

								else

									graph_anal = current_trainee.analytics.where(:time_type => "year", 
											:year => starting.year, :title => er["title"]).first

									graph_btns["#{er["title"]}"] = []

									count_years = years.count

									count_years.times do |i|
										graph_btns["#{er["title"]}"][i] = 0
									end

									if graph_anal
										graph_btns["#{er["title"]}"][current_index] = graph_anal.fbk
									else
										graph_btns["#{er["title"]}"][current_index] = 0
									end
								end
							end
						end
					end

					if (red + yellow + green) != 0
						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
 					
						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
					end

					if (big_red + big_yellow + big_green) != 0
						big_avg = (big_red*1 + big_yellow*2 + big_green*3) / (big_red + big_yellow + big_green)

						big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-big_avg).abs }
					end

					big_graph["big_graph_datapoints"][years.index(y)] = big_fbk

				end

				result << {:year => y, :count => year_entries.count, :feedback => fbk}
			end

			if (real_big_colors[0] + real_big_colors[1] + real_big_colors[2]) != 0
				real_big_avg = (real_big_colors[0]*1 + real_big_colors[1]*2 + real_big_colors[2]*3) / 
					(real_big_colors[0] + real_big_colors[1] + real_big_colors[2])

				real_big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-real_big_avg).abs }
			end

			big_graph["fbk_color"] = real_big_fbk

			Time.zone = current_timezone

			final = {:result => result, :btns => btns, :graph_btns => graph_btns, :big_graph => big_graph, 
				:exc_btns => excluded_buttons}

			render :json => final
		else
			render :json => {:message => "User not found"}, :status => 404
		end 
	end

	def home_month
		if current_trainee

			current_timezone = Time.zone
			Time.zone = "UTC"

			exc = []

			if params["except"]
				exc = params["except"]
			end

			result = []
			btns   = {}
			graph_btns = {}

			excluded_buttons = {}
			big_graph = {}

			big_graph["big_graph_datapoints"] = [0,0,0,0,0,0,0,0,0,0,0,0]

			year = params["year"]

			months = [1,2,3,4,5,6,7,8,9,10,11,12]

			starting = Time.zone.parse("#{year}-01-01")
			ending   = Time.zone.parse("#{year}-12-31") + 1.day

			year_entries = Entry.where(:trainee_email => current_trainee.email, :created_at.gt => starting,
			 :created_at.lt => ending).not_in(:template_id => current_trainee.hidden_templates)

			real_big_fbk = 0
			real_big_colors = [0.0, 0.0, 0.0]

			months.each do |m|

				fbk = 0

				big_fbk = 0

				m_starting = Time.zone.parse("#{year}-#{m}-01")
				m_ending   = Time.zone.parse("#{year}-#{m}-#{m_starting.end_of_month.day}") + 1.day

				month_entries = year_entries.where(:created_at.gt => m_starting, :created_at.lt => m_ending)

				fbk_entries = month_entries.where(:feedback => true)

				if fbk_entries.count != 0

					red = 0.0
					green = 0.0
					yellow = 0.0

					big_red = 0.0
					big_green = 0.0
					big_yellow = 0.0
					
					fbk_entries.each do |e|
						e.entry_fields["responses"].each do |er|
							if er["active"] == "1"

								er["subs"].each do |sub|
									if sub["active"] == "1"
										if sub["feedback"] == "1"
											big_red += 1
											real_big_colors[0] += 1
										elsif sub["feedback"] == "2"
											big_yellow += 1
											real_big_colors[1] += 1
										elsif sub["feedback"] == "3"
											big_green += 1
											real_big_colors[2] += 1
										end
									end
								end
								
								if er["title"].in?(exc) == false
									if !btns.has_key?(er["title"])
										anal = current_trainee.analytics.where(:time_type => "year", :year => starting.year,
										 :title => er["title"]).first

										if anal
											btns["#{er["title"]}"] = anal.fbk
										end
									end

									er["subs"].each do |sub|
										if sub["active"] == "1"
											if sub["feedback"] == "1"
												red += 1
											elsif sub["feedback"] == "2"
												yellow += 1
											elsif sub["feedback"] == "3"
												green += 1
											end
										end
									end
								else

									if !btns.has_key?(er["title"])
										btns["#{er["title"]}"] = -1
									end

									if !excluded_buttons.has_key?(er["title"])
										exc_anal = current_trainee.analytics.where(:time_type => "year", :year =>
										 starting.year, :title => er["title"]).first

										if exc_anal
											excluded_buttons["#{er["title"]}"] = exc_anal.fbk
										end
									end

								end

								if graph_btns.has_key?("#{er["title"]}")

									if graph_btns["#{er["title"]}"][m-1] == 0

										graph_anal = current_trainee.analytics.where(:time_type => "month", 
											:year => starting.year, :month => m, :title => er["title"]).first

										if graph_anal
											graph_btns["#{er["title"]}"][m-1] = graph_anal.fbk
										else
											graph_btns["#{er["title"]}"][m-1] = 0
										end
									end

								else

									graph_anal = current_trainee.analytics.where(:time_type => "month", 
											:year => starting.year, :month => m, :title => er["title"]).first

									graph_btns["#{er["title"]}"] = [0,0,0,0,0,0,0,0,0,0,0,0]

									if graph_anal
										graph_btns["#{er["title"]}"][m-1] = graph_anal.fbk
									else
										graph_btns["#{er["title"]}"][m-1] = 0
									end

								end

							end
						end
					end

					if (red + yellow + green) != 0
						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
					
						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
					end

					if (big_red + big_yellow + big_green) != 0
						big_avg = (big_red*1 + big_yellow*2 + big_green*3) / (big_red + big_yellow + big_green)

						big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-big_avg).abs }
					end

					big_graph["big_graph_datapoints"][m-1] = big_fbk

				end

				result << {:year => year, :month => m, :count => month_entries.count, :feedback => fbk}
			end

			if (real_big_colors[0] + real_big_colors[1] + real_big_colors[2]) != 0
				real_big_avg = (real_big_colors[0]*1 + real_big_colors[1]*2 + real_big_colors[2]*3) / 
					(real_big_colors[0] + real_big_colors[1] + real_big_colors[2])

				real_big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-real_big_avg).abs }
			end

			big_graph["fbk_color"] = real_big_fbk

			overall_fbk = current_trainee.feedbacks.where(:type => "year", :year => year)

			Time.zone = current_timezone

			final = {:result => result, :btns => btns, :graph_btns => graph_btns, :big_graph => big_graph, 
				:exc_btns => excluded_buttons}

			final[:overall_fbk] = []

			if overall_fbk

				overall_fbk.each do |f|
					t = Trainer.find(f.trainer)
					if f.text != "" && f.text != nil
						final[:overall_fbk] << {:trainer_fbk => f.as_json, :trainer => "#{t.f_name} #{t.l_name}"}
					end
				end
			end

			render :json => final			
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def home_day
		if current_trainee

			current_timezone = Time.zone
			Time.zone = "UTC"

			exc = []

			if params["except"]
				exc = params["except"]
			end

			result = []
			btns   = {}
			excluded_buttons = {}
			graph_btns = {}

			big_graph = {}

			big_graph["big_graph_datapoints"] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


			year  =  params["year"]

			month =  params["month"]

			day_count = Time.days_in_month(month.to_i, year.to_i)

			all_entries = Entry.where(:trainee_email => current_trainee.email).not_in(:template_id => current_trainee.hidden_templates)

			real_big_fbk = 0
			real_big_colors = [0.0, 0.0, 0.0]

			(1..day_count).each do |d|

				fbk = 0

				big_fbk = 0

				starting = Time.zone.parse("#{year}-#{month}-#{d}")

				ending   = Time.zone.parse("#{year}-#{month}-#{d}") + 1.day

				entries = all_entries.where(:created_at.gt => starting, :created_at.lt => ending)

				fbk_entries = entries.where(:feedback => true)

				if fbk_entries.count != 0

					red = 0.0
					green = 0.0
					yellow = 0.0

					big_red = 0.0
					big_green = 0.0
					big_yellow = 0.0
					
					fbk_entries.each do |e|
						e.entry_fields["responses"].each do |er|
							if er["active"] == "1"

								er["subs"].each do |sub|
									if sub["active"] == "1"
										if sub["feedback"] == "1"
											big_red += 1
											real_big_colors[0] += 1
										elsif sub["feedback"] == "2"
											big_yellow += 1
											real_big_colors[1] += 1
										elsif sub["feedback"] == "3"
											big_green += 1
											real_big_colors[2] += 1
										end
									end
								end

								if er["title"].in?(exc) == false
									if !btns.has_key?(er["title"])
										anal = current_trainee.analytics.where(:time_type => "month", :month => starting.month,
									 	 :year => starting.year, :title => er["title"]).first

										if anal
											btns["#{er["title"]}"] = anal.fbk
										end
									end

									er["subs"].each do |sub|
										if sub["active"] == "1"
											if sub["feedback"] == "1"
												red += 1
											elsif sub["feedback"] == "2"
												yellow += 1
											elsif sub["feedback"] == "3"
												green += 1
											end
										end
									end
								else

									if !btns.has_key?(er["title"])
										btns["#{er["title"]}"] = -1
									end

									if !excluded_buttons.has_key?(er["title"])
										exc_anal = current_trainee.analytics.where(:time_type => "month", 
											:month => starting.month, :year => starting.year, :title => er["title"]).first

										if exc_anal
											excluded_buttons["#{er["title"]}"] = exc_anal.fbk
										end
									end

								end

								if graph_btns.has_key?("#{er["title"]}")

									if graph_btns["#{er["title"]}"][d-1] == 0

										graph_anal = current_trainee.analytics.where(:time_type => "day", 
											:year => starting.year, :month => starting.month, :day => d, 
											:title => er["title"]).first

										if graph_anal
											graph_btns["#{er["title"]}"][d-1] = graph_anal.fbk
										else
											graph_btns["#{er["title"]}"][d-1] = 0
										end
									end

								else

									graph_anal = current_trainee.analytics.where(:time_type => "day", 
											:year => starting.year, :month => starting.month, :day => d, 
											:title => er["title"]).first

									graph_btns["#{er["title"]}"] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

									if graph_anal
										graph_btns["#{er["title"]}"][d-1] = graph_anal.fbk
									else
										graph_btns["#{er["title"]}"][d-1] = 0
									end

								end

							end
						end
					end

					if (red + yellow + green) != 0
						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)

						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
					end

					if (big_red + big_yellow + big_green) != 0
						big_avg = (big_red*1 + big_yellow*2 + big_green*3) / (big_red + big_yellow + big_green)

						big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-big_avg).abs }
					end

					big_graph["big_graph_datapoints"][d-1] = big_fbk

				end

				result << {:year => year, :month => month, :day => d, :count => entries.count, :feedback => fbk}

			end

			if (real_big_colors[0] + real_big_colors[1] + real_big_colors[2]) != 0
				real_big_avg = (real_big_colors[0]*1 + real_big_colors[1]*2 + real_big_colors[2]*3) / 
					(real_big_colors[0] + real_big_colors[1] + real_big_colors[2])

				real_big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-real_big_avg).abs }
			end

			big_graph["fbk_color"] = real_big_fbk

			overall_fbk = current_trainee.feedbacks.where(:type => "month", :month => month, :year => year)
			
			Time.zone = current_timezone

			final = {:result => result, :btns => btns, :graph_btns => graph_btns, :big_graph => big_graph, 
				:exc_btns => excluded_buttons}

			final[:overall_fbk] = []

			if overall_fbk

				overall_fbk.each do |f|
					t = Trainer.find(f.trainer)
					if f.text != "" && f.text != nil
						final[:overall_fbk] << {:trainer_fbk => f.as_json, :trainer => "#{t.f_name} #{t.l_name}"}
					end
				end
			end

			render :json => final

		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end 

	def list_day_entries
		if current_trainee

			current_timezone = Time.zone
			Time.zone = "UTC"

			exc = []

			if params["except"]
				exc = params["except"]
			end

			result = []
			btns   = {}

			starting = Time.zone.parse(params["date"])
			ending   = Time.zone.parse(params["date"]) + 1.day

			entries = Entry.where(:trainee_email => current_trainee.email, :created_at.gt => starting,
			 :created_at.lt => ending).not_in(:template_id => current_trainee.hidden_templates).asc(:created_at)

			entries.each do |e|

				fbk = 0

				if e.feedback
					
					red = 0.0
					green = 0.0
					yellow = 0.0

					e.entry_fields["responses"].each do |er|
						if er["active"] == "1"

							if er["title"].in?(exc) == false
								if !btns.has_key?(er["title"])
									anal = current_trainee.analytics.where(:time_type => "day", :day => starting.day,
								 	 :month => starting.month, :year => starting.year, :title => er["title"]).first

									if anal
										btns["#{er["title"]}"] = anal.fbk
									end
								end
								er["subs"].each do |sub|
									if sub["active"] == "1"
										if sub["feedback"] == "1"
											red += 1
										elsif sub["feedback"] == "2"
											yellow += 1
										elsif sub["feedback"] == "3"
											green += 1
										end
									end
								end
							else
								if !btns.has_key?(er["title"])
									btns["#{er["title"]}"] = -1
								end
							end
						end
					end

					if (red + yellow + green) != 0
						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)

						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
					end
				end

				result << {:entry => e.as_json.except("file_file_name", "file_content_type", "file_file_size", "file_updated_at",
						   "file_fingerprint"), :file => {:med => e.file.url(:medium), :org => e.file.url},
						   :template => e.template_name, :feedback => fbk}
			end

			overall_fbk = current_trainee.feedbacks.where(:type => "day", :day => starting.day, :month => starting.month,
			 :year => starting.year)
			
			Time.zone = current_timezone

			final = {:result => result, :btns => btns}

			final[:overall_fbk] = []

			if overall_fbk

				overall_fbk.each do |f|
					t = Trainer.find(f.trainer)
					if f.text != "" && f.text != nil
						final[:overall_fbk] << {:trainer_fbk => f.as_json, :trainer => "#{t.f_name} #{t.l_name}"}
					end
				end
			end

			render :json => final

		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def followTrainer
		if current_trainee

			flag = true

			erronous_email = ""

			existing_trainers = current_trainee.trainers

			existing_trainers.each do |t|
				current_trainee.trainers.delete(t)
			end

			params[:trainer].each do |t|
				trainer = Trainer.where(:email => t).first

				if !trainer.nil?		
					current_trainee.trainers.push(trainer)
				else
					flag = false
					erronous_email = t
				end
			end

			if flag
				render :json => {:message => "Successfully follwed trainers"}, :status => 201
			else
				render :json => {:message => "Trainer not found", :email => erronous_email}, :status => 404
			end
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def delete_trainers
		params[:idz].each do |t|
			t = Trainer.find(t)
			if t
				current_trainee.trainers.delete(t)				
			end
		end

		render :json => {:message => "Successfully deleted trainers"}, :status => 201
	end

	def test_notif
			entry = Entry.last
			trainer = Trainer.first
			t = "ebf439103c6dd5e38084d95b2d2dfaa2b88616d3b5218474e61e0d7254c59b0d"
			pusher = Grocer.pusher(
		      certificate: "/var/www/rails1/shared/certs/pushcert.pem",
		      passphrase:  "",                       
		      gateway:     "gateway.sandbox.push.apple.com",
		    )
		    notification = Grocer::Notification.new(
		        device_token:      t,
		        alert:             "Hello Trainee from Grocer!",
		        custom:            entry.id.as_json,
		        expiry:            Time.now + 60*60
		    )
		    pusher.push(notification)

		render :json => {:djgfj => "djshfd"}, :status => 200
	end

	def template_id_params
		params.require(:template).permit(:template_id)
	end

	def entry_params
		params.require(:entry).permit!
	end

	def trainee_params
		params.require(:trainee).permit(:email, :password, :token, :f_name, :l_name, :geo, :radius)   	
    end

    def trainer_follow_params
    	params.require(:trainer).permit!
    end

    def blocked_params
    	params.require(:blockedItems).permit!
    end

    def except_params 
    	params.require(:except).permit!
    end

    def register_push_params
    	params.require(:push).permit(:push_token)
    end
end
