class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def trainer_token_authentication
  	klass=params.keys.first
  	#p params,params[klass]["token"]
  	@trainer=Trainer.where(:token => params["trainer"]["token"]).first
  	#p params[klass]["token"], @trainer.token
    if @trainer
    	if @trainer.token == params["trainer"]["token"]
    		sign_in @trainer, store: false 
    	else
    		render :json => {:message => "Invalid token"}, :status => 401
    	end
    else
      render :json => {:message => "User not found"}, :status => 404
    end
  end

  def trainee_token_authentication
    #p params,params[klass]["token"]
    @trainee=Trainee.where(:token => params["trainee"]["token"]).first
    #p params[klass]["token"], @trainer.token
    if @trainee
      if @trainee.token == params["trainee"]["token"]
        sign_in @trainee, store: false 
      else
        render :json => {:message => "Invalid token"}, :status => 401
      end
    else
      render :json => {:message => "User not found"}, :status => 404
    end
  end

  def token_authentication

    klass = ""

    if params.has_key?(:trainer)
      klass = "trainer"
    elsif params.has_key?(:trainee)
      klass = "trainee"
    else
      render :json => {:message => "Invalid Creds"}, :status => 401
    end

    @user = eval(klass.classify).where(:token => params[klass]["token"]).first

    if @user
      if @user.token == params[klass]["token"]
        sign_in @user, store: false 
      else
        render :json => {:message => "Invalid token"}, :status => 401
      end
    else
      render :json => {:message => "User not found"}, :status => 404
    end
    
  end

  def user_not_found
    render :json => {:message => "User not found"}, :status => 404
  end

  def trainer_not_found
    render :json => {:message => "Trainer not found"}, :status => 404
  end

  def trainer_is_owner
    render :json => {:message => "Trainer is already owner of this template"}, :status => 403
  end

  def already_shared
    render :json => {:message => "Already shared with trainer"}, :status => 403
  end

  def trainer_not_followed
    render :json => {:message => "Have to follow trainer first"}, :status => 403
  end

  def template_not_found
    render :json => {:message => "Template not found"}, :status => 404
  end

  def throw_error(error)
    render :json => {:error => error.message}, :status => 400
  end 
  
  def welcom
  end

before_filter :add_allow_credentials_headers

def add_allow_credentials_headers                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
  response.headers['Access-Control-Allow-Origin'] = request.headers['Origin'] || '*'                                                                                                                                                                                                     
  response.headers['Access-Control-Allow-Credentials'] = 'true'                                                                                                                                                                                                                          
end 

def options                                                                                                                                                                                                                                                                              
  head :status => 200, :'Access-Control-Allow-Headers' => 'accept, content-type'                                                                                                                                                                                                         
end

end
