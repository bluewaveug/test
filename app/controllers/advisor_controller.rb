class AdvisorController < ApplicationController
	require "#{Rails.root}/lib/token_generator"
	before_filter :trainer_token_authentication

	skip_before_action :verify_authenticity_token

	def login
		@trainer = Trainer.new
	end
	
	def profil
		render layout: "pagecompleteTrainer"
	end

end
