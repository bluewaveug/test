class TrainerController < ApplicationController
	require "#{Rails.root}/lib/token_generator"
	before_filter :trainer_token_authentication, :except => [:registration, :signIn]
	skip_before_action :verify_authenticity_token
	#rescue_from Exception, :with => :throw_error
	#rescue_from Mongoid::Errors::Validations, :with => :throw_error
	
	def registration
		if Trainer.where(:email => params["trainer"]["email"]).first == nil
			
			trainer=Trainer.create trainer_params
			token=TokenGenerator.generate

			invoiceFree=Invoice.create
						  				   				invoiceFree.trainer=trainer
			package=Package.where(:type=>1).first
			invoiceFree.title=package.title
			invoiceFree.maxUsers=package.max_users

			time = Time.new
			invoiceFree.date_begin=time
			invoiceFree.date_end=time + 1.months
			
			invoiceFree.next=true

			trainer.package=package.title
			invoiceFree.update_attributes

			if params["trainer"]["loc"].map(&:to_f) != [0.0, 0.0]
				trainer.update_attributes :token => token, :loc => params["trainer"]["loc"].map(&:to_f)
			else
				trainer.update_attributes :token => token
			end
			if trainer
				render :json => {:message => "Successfully created", :trainer_id =>trainer.id.to_s, :token => token}, :status => 201
			else
				render :json => {:message => "Nope"}, :status => 500
			end
		else
			render :json => {:message => "Email alerady exists"}, :status => 401
		end
	
	end

	def signIn
		trainer=Trainer.where(:email => params["trainer"]["email"]).first
		if trainer
			if trainer.valid_password?(params["trainer"]["password"])
				token=TokenGenerator.generate
				if params["trainer"]["loc"].map(&:to_f) != [0.0, 0.0]
					trainer.update_attributes :token => token, :loc => params["trainer"]["loc"].map(&:to_f)
				else
					trainer.update_attributes :token => token
				end
				render :json => {:message => "Successfully login", :trainer_id => trainer.id.to_s, :token => token}
				
			else
				render :json => {:message => "Wrong creds1"}, :status => 400
			end
		else
			render :json => {:message => "Wrong creds2"}, :status => 404
		end
	end
	def profil
		@invoice=current_trainer.invoices.last
		@packages=[]
		@tabPrice=[]
		@discount=[]
		Package.all.each do |t|
			@tabPrice<< [t.type , t.price]
      			@packages<< [t.title , t.type]
		end
		Discount.all.each do |t|
			@discount<< t.percentage
		end
		render layout: "pagecompleteTrainer"
	end
	
	def listAllTrainees
		@trainees = []
		@trainees=current_trainer.trainees
		render :json => @trainees
	end
	def afficherTrainees
		render layout: "pagecompleteTrainer"
	end
	
	def changepassword
		render layout: "pagecompleteTrainer"
	end

	def options
		@packages=[]
		Package.all.each do |t|
      			@packages<< [t.title , t.type]
		end
		render layout: "pagecompleteTrainer"
	end

	def saveInvoice

		invoiceOld=current_trainer.invoices.last
		if invoiceOld.next==true
			invoiceNew=Invoice.create
		else
			invoiceOlds=[]
			invoiceOlds=current_trainer.invoices
			invoiceOlds.each do |t|
				if t.next=false
					invoiceOld=t
				end
			end
			invoiceNew=current_trainer.invoices.last
		end
		discountAdd=[]
		Discount.all.each do |t|
			discountAdd<< t.percentage
		end
		
		invoiceNew.trainer=current_trainer
		package=Package.where(:type=>params["package"]).first
		invoiceNew.title=package.title
		invoiceNew.maxUsers=package.max_users
		invoiceNew.price=package.price
	
		invoiceNew.date_begin=Time.now
		if invoiceOld.next==false
			if Time.now < invoiceOld.date_begin
				invoiceNew.date_begin=invoiceOld.date_begin
			end
		else
			if Time.now < invoiceOld.date_end
				invoiceNew.date_begin=invoiceOld.date_end + 1.days
			end
		end
		
		if invoiceOld.price < package.price
			invoiceNew.discountPackageUp=discountAdd[1]
		end

		if params["prepaid"]=="true"
			invoiceNew.date_end=invoiceNew.date_begin + 1.years
			invoiceNew.discountPrePaid=discountAdd[0]
			invoiceNew.total=12*(package.price*(100 - invoiceNew.discountPrePaid - invoiceNew.discountPackageUp))/100
		else
			invoiceNew.date_end=invoiceNew.date_begin + 1.months
			invoiceNew.total=package.price*(100  -invoiceNew.discountPackageUp)/100
		end

		invoiceNew.update_attributes
		trainer=Trainer.where(:email => current_trainer.email).first
		trainer.package=invoiceNew.title
		trainer.update_attributes
		render :json => {:message => "package saved"}
end
		
	def update_loc
		if current_trainer
			if params["trainer"]["loc"].map(&:to_f) != [0.0, 0.0]
				current_trainer.update_attributes!(:loc => params["trainer"]["loc"].map(&:to_f))
			end
			render :json => {:message => "Trainer location updated"}, :status => 201
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def which_screen_to_jump_to
		if current_trainer

			templates = current_trainer.templates.distinct(:id)
			
			trainee   = Trainee.where(:email => params["trainee"]["email"]).first
			
			shared = trainee.shares.where(:trainer_id => current_trainer.id.to_s).distinct(:template_id)
			
			templates << shared
			
			templates.flatten!
			
			templates.uniq!

			total = Entry.where(:trainee_email => params["trainee"]["email"]).any_in(:template_id => templates).asc(:created_at)

			screen = "none"
			
			year  = "none"
			month = "none"
			day   = "none"

			if total.count != 0

				first = total.first.created_at#.to_time.to_i

				last  = total.last.created_at#.to_time.to_i

				#diff = (last - first) / 86400

				if last.year == first.year
					if last.month == first.month
						if last.day == first.day
							# Only one day
							screen = "day"
							year   = first.year
							month  = first.month
							day    = first.day
						else	
							# Only one day's worth of entries
							screen = "days"
							year   = first.year
							month  = first.month
						end
					else
						# More tahn one month
						screen = "months"
						year   = first.year
					end
				else
					# More than one year
					screen = "years"
				end
					
			end

			render :json => {:screen => screen, :year => year, :month => month, :day => day}, :status => 200
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def signOut
		current_trainer.update_attributes :token => nil
		render :json => {:message => "log out log out"}
	end

	def listTrainees

		if current_trainer.trainees?
			
			traineez = current_trainer.trainees.as_json

			traineez.each do |t|
				trnee = Trainee.find(t["_id"])
				alltm = trnee.alltimes.where(:trainer_id => current_trainer.id.to_s).first
				if alltm
					fbk = alltm.fbk
				else
					fbk = 0 
				end
				t.merge!(:fbk => fbk)

				templates = current_trainer.templates.distinct(:id)
				
				shared = trnee.shares.where(:trainer_id => current_trainer.id.to_s).distinct(:template_id)
				
				templates << shared
				
				templates.flatten!
				
				templates.uniq!

				count = Entry.where(:trainee_email => trnee.email).any_in(:template_id => templates).count

				t.merge!(:count => count)
			end

			render :json => {:owns => traineez, :shared => {}}
		else
			render :json => {:message => "No trainees found"}, :status => 404
		end
	end

	def assign_them_templates
		trainer_templates = current_trainer.templates.distinct(:id).map &->(x){x.to_s}

		params[:templates].each do |key, tt|

			# Rails.logger.info "The are the templates params #{key} #{tt}"

			trainee = Trainee.where(:email => key).first

			other_templates = trainee.templates_array - trainer_templates

			tt.each do |t|
				if t != "6969"
					other_templates << t["$oid"]
				end
			end

			trainee.update_attributes!(:templates_array => other_templates)
		end

		render :json => {:message => "Updated templates array for trainee"}, :status => 201
	end

	def assign_user_templates
		trainer_templates = current_trainer.templates.distinct(:id).map &->(x){x.to_s}

		params[:templates].each do |key, tt|

			# Rails.logger.info "The are the templates params #{key} #{tt}"

			trainee = Trainee.where(:email => key).first

			other_templates = trainee.templates_array - trainer_templates

			tt.each do |t|
				if t != "6969"
					other_templates << t["$oid"]
				end
			end

			trainee.update_attributes!(:templates_array => other_templates)
		end

		render :json => {:message => "Updated templates array for trainee"}, :status => 201

	end

	def list_trainees_with_templates_for_settings

		traineez = current_trainer.trainees.as_json
		trainer_templates_idz = current_trainer.templates.distinct(:id).map &->(x){ x.to_s }

		traineez.each do |t|
			t_assigned = trainer_templates_idz & t["templates_array"]
			tes = []
			unassigned_temps = trainer_templates_idz - t_assigned
			u_tes = []

			t_assigned.each do |x|
				xt = Template.find(x).as_json
				if xt
					# if t["hidden_templates"].index(xt["_id"].to_s) == nil 
					# 	xt.merge!(:hidden => "nope")
					# else
					# 	xt.merge!(:hidden => "yesh")
					# end
					tes << xt
				end
			end
			t.merge!(:trainer_templates => tes)

			unassigned_temps.each do |ut|
				ut = Template.find(ut)
				if ut
					u_tes << ut
				end
			end
			t.merge!(:trainer_unassigned => u_tes)
		end

		render :json => {:traineez => traineez, :all_templates => current_trainer.templates.as_json}, :status => 200
	end

	def list_selected_trainee_with_templates
		t = Trainee.where(:email => params["trainee"]["email"]).first.as_json
		trainer_templates_idz = current_trainer.templates.distinct(:id).map &->(x){ x.to_s }

		t_assigned = trainer_templates_idz & t["templates_array"]
		tes = []
		unassigned_temps = trainer_templates_idz - t_assigned
		u_tes = []

		t_assigned.each do |x|
			xt = Template.find(x).as_json
			if xt
				tes << xt
			end
		end
		t.merge!(:trainer_templates => tes)

		unassigned_temps.each do |ut|
			ut = Template.find(ut)
			if ut
				u_tes << ut
			end
		end
		t.merge!(:trainer_unassigned => u_tes)

		render :json => {:trainee => t}, :status => 200
	end

	def trainer_params
		params.require(:trainer).permit(:email, :password, :token, :f_name, :l_name, :loc)   	
	end
	
	def trainer_search
		if current_trainer
			
			templates = current_trainer.templates.distinct(:id)
			
			shared = Share.where(:trainer_id => current_trainer.id.to_s).distinct(:template_id)
			
			templates << shared
			
			templates.flatten!
			
			templates.uniq!

			if params["client"] == "fuckit"

				all_entries = Entry.any_in(:template_id => templates)
			else
				trn = Trainee.find(params["client"])

				if params["starting"] == "history" && params["ending"] == "history"

					all_entries = Entry.where(:trainee_email => trn.email).any_in(:template_id => templates)
				else
					current_timezone = Time.zone
					Time.zone = "UTC"

					starting = Time.zone.parse(params["starting"])
					ending   = Time.zone.parse(params["ending"])

					all_entries = Entry.where(:trainee_email => trn.email, :created_at.gt => starting,
					 :created_at.lt => ending).any_in(:template_id => templates)

					Time.zone = current_timezone
				end
			end


			# all_entries = Entry.any_in(:template_id => templates)
			
			result = all_entries.any_of(:search_keywords => /#{params["query"].downcase}/).desc(:created_at).limit(10).offset(params["offset"].to_i)
			
			result_query = []
			
			result.each do |re|
				
				fbk = 0

				if re.feedback
					
					red = 0.0
					green = 0.0
					yellow = 0.0

					re.entry_fields["responses"].each do |er|
						if er["active"] == "1"
							er["subs"].each do |sub|
								if sub["active"] == "1"
									if sub["feedback"] == "1"
										red += 1
									elsif sub["feedback"] == "2"
										yellow += 1
									elsif sub["feedback"] == "3"
										green += 1
									end
								end
							end
						end
					end

					if (red + yellow + green) != 0
						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)

						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
					end
				end
				
				result_query << {:entry => re.as_json.except("file_file_name", "file_content_type", "file_file_size", "file_updated_at",
					"file_fingerprint"), :file => {:med => re.file.url(:medium), :org => re.file.url}, 
					:template => re.template_name, :feedback => fbk}
					
			end
			
			render :json => {:result => result_query}, :status => 200
			
		else
			render :json => {:message => "Unauthorized"}, :status => 401
		end
		
	end

    def create_template
    	if current_trainer
	    	template=current_trainer.templates.create template_params

	    	frequency_string = params["frequency_string"]

	    	if frequency_string == "Always"

	    		Rails.logger.info "Frequency string was Always"

	    		template.update_attributes!(:frequency => "Always", :frequency_type => "Always")
	    	else
	    		frequency      = frequency_string.split(" x ")[0]
	    		frequency_type = frequency_string.split(" x ")[1]

	    		template.update_attributes!(:frequency => frequency, :frequency_type => frequency_type)
	    	end

	    	# if template.template_fields == nil
	    		
	    	# end

	    	render :json => {:message => "Successfully created", :template_id => template.id.to_s}, :status => 201
	    else
	    	render :json => {:message => "Unauthorized"}, :status => 401
	    end
    end

    def edit_template
    	if current_trainer
	    	template=Template.where(:id => params["template"]["template_id"]).first
	    	if template
	    		template.update_attributes template_params.reject!{ |k| k == "template_id" }

	    		frequency_string = params["frequency_string"]

		    	if frequency_string == "Always"

		    		template.update_attributes!(:frequency => "Always", :frequency_type => "Always")
		    	else

		    		frequency      = frequency_string.split(" x ")[0]
		    		frequency_type = frequency_string.split(" x ")[1]

		    		template.update_attributes!(:frequency => frequency, :frequency_type => frequency_type)
		    	end

	    		render :json => {:message => "Successfully Edited", :template_id => template.id.to_s}, :status => 201
	    	else
	    		render :json => {:message => "Template not found"}, :status => 404
	    	end
		else
	    	render :json => {:message => "Unauthorized"}, :status => 401
	    end
    end

    def delete_template
    	if current_trainer
	    	template=Template.where(:id => params["template"]["template_id"]).first

	    	if template
	    		template.delete
	    		render :json => {:message => "Successfully deleted"}, :status => 201
	    	else
	    		render :json => {:message => "Template not found"}, :status => 404
	    	end
		else
	    	render :json => {:message => "Unauthorized"}, :status => 401
	    end
    end

    def subscribe_to_template
    	if current_trainer
    		template=Template.where(:id => params["template"]["template_id"]).first
    		if template
    			trainee = Trainee.where(:email => params["trainee"]["email"]).first
    			if trainee 
    				existing = trainee.templates_array
    				if existing.include?(template.id.to_s)
    					render :json => {:message => "Already subscribed"}, :status => 409
    				else
    					existing << template.id.to_s
    					trainee.update_attributes!(:templates_array => existing)
    					render :json => {:message => "Subscribed"}, :status => 201
    				end
    			else
    				render :json => {:message => "Trainee not found"}, :status => 404
    			end
    		else
    			render :json => {:message => "Template not found"}, :status => 404
    		end
    	else
    		render :json => {:message => "Unauthorized"}, :status => 401
    	end
    end

    def register_for_push
		if current_trainer

			real_token = register_push_params["push_token"]

			real_token.slice!("<")

			real_token.slice!(">")

			real_token.gsub!(" ", "")

			existing_devices = Trainerdevice.where(:push_token => real_token)

			if existing_devices.count != 0
				existing_devices.delete_all
			end

			current_trainer.trainerdevices.delete_all

			if current_trainer.trainerdevices.create!(:push_token => real_token)
				render :json => {:message => "Added device for trainer"}, :status => 200
			else
				render :json => {:message => "OOps"}, :status => 500
			end
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def overall_feedback

		trnee = Trainee.where(:email => params["trainee"]["email"]).first

		if trnee

			year = ""
			month = ""
			day = ""

			if feedback_params[:year]
				year = feedback_params[:year]
			end

			if feedback_params[:month]
				month = feedback_params[:month]
			end

			if feedback_params[:day]
				day = feedback_params[:day]
			end

			fbk   =  trnee.feedbacks.where(:type => feedback_params[:type], :year => year, :month => month, :day => day,
			 :trainer => current_trainer.id.to_s).first

			if fbk
				fbk.update_attributes!(:text => feedback_params[:text])
			else
				trnee.feedbacks.create!(:type => feedback_params[:type], :year => year, :month => month, :day => day,
				 :text => feedback_params[:text], :trainer => current_trainer.id.to_s)
			end

			render :json => {:message => "Feedback added"}, :status => 201
		else
			render :json => {:message => "Trainee not found"}, :status => 404
		end

	end

	def week_graph_fbk

		current_timezone = Time.zone
		Time.zone = "UTC"

		exc = []

		if params["except"]
			exc = params["except"]
		end

		btns = {}

		templates = current_trainer.templates.distinct(:id)

		trainee   = Trainee.where(:email => params["trainee"]["email"]).first
		
		shared = trainee.shares.where(:trainer_id => current_trainer.id.to_s).distinct(:template_id)
		
		templates << shared
		
		templates.flatten!
		
		templates.uniq!

		all_entries = Entry.where(:trainee_email => params["trainee"]["email"], :created_at.gt => Time.zone.parse(params["starting"]), 
			:created_at.lt => Time.zone.parse(params["ending"]) + 1.day).any_in(:template_id => templates)

		fbk_entries = all_entries.where(:feedback => true)

		starting = Date.parse(params["starting"])
		ending   = Date.parse(params["ending"])

		range_array = (starting..ending).map{|d| [d.day, d.month, d.year]}.uniq

		if fbk_entries.count != 0
			
			fbk_entries.each do |e|
				e.entry_fields["responses"].each do |er|

					if er["active"] == "1"
						
						if er["title"].in?(exc) == false
							if !btns.has_key?(er["title"])

								red = 0.0
								green = 0.0
								yellow = 0.0

								fbk = 0.0

								range_array.each do |rd|
									anal = trainee.analytics.where(:time_type => "day", :day => rd[0], :month => rd[1], :year => rd[2], :title => er["title"]).first

									if anal
										red += anal.red
										green += anal.green
										yellow += anal.yellow
									end
								end

								if (red + yellow + green) != 0

									avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
								
									fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
								end
								
								btns["#{er["title"]}"] = fbk
							end
						else
							if !btns.has_key?(er["title"])
								btns["#{er["title"]}"] = -1
							end
						end
					end
				end
			end
		end

		Time.zone = current_timezone

		final = {:btns => btns}

		render :json => final

	end

	def month_graph_fbk

		current_timezone = Time.zone
		Time.zone = "UTC"

		exc = []

		if params["except"]
			exc = params["except"]
		end

		btns = {}

		templates = current_trainer.templates.distinct(:id)

		trainee   = Trainee.where(:email => params["trainee"]["email"]).first
		
		shared = trainee.shares.where(:trainer_id => current_trainer.id.to_s).distinct(:template_id)
		
		templates << shared
		
		templates.flatten!
		
		templates.uniq!

		all_entries = Entry.where(:trainee_email => params["trainee"]["email"], :created_at.gt => Time.zone.parse(params["starting"]), 
			:created_at.lt => Time.zone.parse(params["ending"]) + 1.day).any_in(:template_id => templates)

		fbk_entries = all_entries.where(:feedback => true)

		starting = Date.parse(params["starting"])
		ending   = Date.parse(params["ending"])

		range_array = (starting..ending).map{|d| [d.month, d.year]}.uniq

		if fbk_entries.count != 0
			
			fbk_entries.each do |e|
				e.entry_fields["responses"].each do |er|

					if er["active"] == "1"
						
						if er["title"].in?(exc) == false
							if !btns.has_key?(er["title"])

								red = 0.0
								green = 0.0
								yellow = 0.0

								fbk = 0.0

								range_array.each do |rd|
									anal = trainee.analytics.where(:time_type => "month", :month => rd[0], :year => rd[1], :title => er["title"]).first

									if anal
										red += anal.red
										green += anal.green
										yellow += anal.yellow
									end
								end

								if (red + yellow + green) != 0

									avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
								
									fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
								end
								
								btns["#{er["title"]}"] = fbk
							end
						else
							if !btns.has_key?(er["title"])
								btns["#{er["title"]}"] = -1
							end
						end
					end
				end
			end
		end

		Time.zone = current_timezone

		final = {:btns => btns}

		render :json => final

	end

    def home_year
		if current_trainer

			current_timezone = Time.zone
			Time.zone = "UTC"

			exc = []

			if params["except"]
				exc = params["except"]
			end

			result = []
			btns = {}
			graph_btns = {}
			excluded_buttons = {}
			big_graph = {}

			big_graph["big_graph_datapoints"] = []

			templates = current_trainer.templates.distinct(:id)

			trainee   = Trainee.where(:email => params["trainee"]["email"]).first
			
			shared = trainee.shares.where(:trainer_id => current_trainer.id.to_s).distinct(:template_id)
			
			templates << shared
			
			templates.flatten!
			
			templates.uniq!

			all_entries = Entry.where(:trainee_email => params["trainee"]["email"]).any_in(:template_id => templates)

			all_dates   = all_entries.distinct(:created_at)

			years = []

			all_dates.each do |d|
				years << d.year
			end

			years.uniq!

			years.count.times do |xtx|
				big_graph["big_graph_datapoints"] << 0
			end

			real_big_fbk = 0
			real_big_colors = [0.0, 0.0, 0.0]

			years.each do |y|

				fbk = 0

				big_fbk = 0

				starting = Time.zone.parse("#{y}-01-01")
				ending   = Time.zone.parse("#{y}-12-31") + 1.day

				year_entries = all_entries.where(:created_at.gt => starting, :created_at.lt => ending)

				fbk_entries = year_entries.where(:feedback => true)

				if fbk_entries.count != 0

					red = 0.0
					green = 0.0
					yellow = 0.0

					big_red = 0.0
					big_green = 0.0
					big_yellow = 0.0
					
					fbk_entries.each do |e|
						e.entry_fields["responses"].each do |er|

							if er["active"] == "1"

								er["subs"].each do |sub|
									if sub["active"] == "1"
										if sub["feedback"] == "1"
											big_red += 1
											real_big_colors[0] += 1
										elsif sub["feedback"] == "2"
											big_yellow += 1
											real_big_colors[1] += 1
										elsif sub["feedback"] == "3"
											big_green += 1
											real_big_colors[2] += 1
										end
									end
								end
								
								if er["title"].in?(exc) == false
									if !btns.has_key?(er["title"])
										anal = trainee.analytics.where(:time_type => "alltime",
										 :title => er["title"]).first

										if anal
											btns["#{er["title"]}"] = anal.fbk
										end
									end

									er["subs"].each do |sub|
										if sub["active"] == "1"
											if sub["feedback"] == "1"
												red += 1
											elsif sub["feedback"] == "2"
												yellow += 1
											elsif sub["feedback"] == "3"
												green += 1
											end
										end
									end
								else

									if !btns.has_key?(er["title"])
										btns["#{er["title"]}"] = -1
									end

									if !excluded_buttons.has_key?(er["title"])
										exc_anal = trainee.analytics.where(:time_type => "alltime",
										 :title => er["title"]).first

										if exc_anal
											excluded_buttons["#{er["title"]}"] = exc_anal.fbk
										end
									end

								end

								current_index = years.index(y)

								if graph_btns.has_key?("#{er["title"]}")

									if graph_btns["#{er["title"]}"][current_index] == 0

										graph_anal = trainee.analytics.where(:time_type => "year", 
											:year => y, :title => er["title"]).first

										if graph_anal
											graph_btns["#{er["title"]}"][current_index] = graph_anal.fbk
										else
											graph_btns["#{er["title"]}"][current_index] = 0
										end
									end

								else

									graph_anal = trainee.analytics.where(:time_type => "year", 
											:year => starting.year, :title => er["title"]).first

									graph_btns["#{er["title"]}"] = []

									count_years = years.count

									count_years.times do |i|
										graph_btns["#{er["title"]}"][i] = 0
									end

									if graph_anal
										graph_btns["#{er["title"]}"][current_index] = graph_anal.fbk
									else
										graph_btns["#{er["title"]}"][current_index] = 0
									end
								end
							end
						end
					end

					if (red + yellow + green) != 0

						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
					
						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
					end

					if (big_red + big_yellow + big_green) != 0
						big_avg = (big_red*1 + big_yellow*2 + big_green*3) / (big_red + big_yellow + big_green)

						big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-big_avg).abs }
					end

					big_graph["big_graph_datapoints"][years.index(y)] = big_fbk
				end

				result << {:year => y, :count => year_entries.count, :feedback => fbk}

			end

			if (real_big_colors[0] + real_big_colors[1] + real_big_colors[2]) != 0
				real_big_avg = (real_big_colors[0]*1 + real_big_colors[1]*2 + real_big_colors[2]*3) / 
					(real_big_colors[0] + real_big_colors[1] + real_big_colors[2])

				real_big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-real_big_avg).abs }
			end

			big_graph["fbk_color"] = real_big_fbk

			Time.zone = current_timezone

			final = {:result => result, :btns => btns, :graph_btns => graph_btns, :big_graph => big_graph, 
				:exc_btns => excluded_buttons}

			render :json => final

		else
			render :json => {:message => "User not found"}, :status => 404
		end 
	end

	def home_month
		if current_trainer

			current_timezone = Time.zone
			Time.zone = "UTC"

			exc = []

			if params["except"]
				exc = params["except"]
			end

			result = []
			btns   = {}
			graph_btns = {}

			excluded_buttons = {}
			big_graph = {}

			big_graph["big_graph_datapoints"] = [0,0,0,0,0,0,0,0,0,0,0,0]
			
			year = params["year"]

			feedbacks_test = [0,1,2,3]

			months = [1,2,3,4,5,6,7,8,9,10,11,12]

			starting = Time.zone.parse("#{year}-01-01")
			ending   = Time.zone.parse("#{year}-12-31") + 1.day

			templates = current_trainer.templates.distinct(:id)

			trainee   = Trainee.where(:email => params["trainee"]["email"]).first
			
			shared = trainee.shares.where(:trainer_id => current_trainer.id.to_s).distinct(:template_id)
			
			templates << shared
			
			templates.flatten!
			
			templates.uniq!

			year_entries = Entry.where(:trainee_email => params["trainee"]["email"], :created_at.gt => starting,
			 :created_at.lt => ending).any_in(:template_id => templates)

			real_big_fbk = 0
			real_big_colors = [0.0, 0.0, 0.0]

			months.each do |m|

				fbk = 0

				big_fbk = 0

				m_starting = Time.zone.parse("#{year}-#{m}-01")
				m_ending   = Time.zone.parse("#{year}-#{m}-#{m_starting.end_of_month.day}") + 1.day

				month_entries = year_entries.where(:created_at.gt => m_starting, :created_at.lt => m_ending)

				fbk_entries = month_entries.where(:feedback => true)

				if fbk_entries.count != 0

					red = 0.0
					green = 0.0
					yellow = 0.0

					big_red = 0.0
					big_green = 0.0
					big_yellow = 0.0
					
					fbk_entries.each do |e|
						e.entry_fields["responses"].each do |er|
							if er["active"] == "1" 

								if er["title"].in?(exc) == false

									er["subs"].each do |sub|
										if sub["active"] == "1"
											if sub["feedback"] == "1"
												big_red += 1
												real_big_colors[0] += 1
											elsif sub["feedback"] == "2"
												big_yellow += 1
												real_big_colors[1] += 1
											elsif sub["feedback"] == "3"
												big_green += 1
												real_big_colors[2] += 1
											end
										end
									end
								
									if !btns.has_key?(er["title"])
										anal = trainee.analytics.where(:time_type => "year", :year => starting.year, 
											:title => er["title"]).first

										if anal
											btns["#{er["title"]}"] = anal.fbk
										end
									end

									er["subs"].each do |sub|
										if sub["active"] == "1"
											if sub["feedback"] == "1"
												red += 1
											elsif sub["feedback"] == "2"
												yellow += 1
											elsif sub["feedback"] == "3"
												green += 1
											end
										end
									end
								else

									if !btns.has_key?(er["title"])
										btns["#{er["title"]}"] = -1
									end

									if !excluded_buttons.has_key?(er["title"])
										exc_anal = trainee.analytics.where(:time_type => "year", :year =>
										 starting.year, :title => er["title"]).first

										if exc_anal
											excluded_buttons["#{er["title"]}"] = exc_anal.fbk
										end
									end

								end

								if graph_btns.has_key?("#{er["title"]}")

									if graph_btns["#{er["title"]}"][m-1] == 0

										graph_anal = trainee.analytics.where(:time_type => "month", 
											:year => starting.year, :month => m, :title => er["title"]).first

										if graph_anal
											graph_btns["#{er["title"]}"][m-1] = graph_anal.fbk
										else
											graph_btns["#{er["title"]}"][m-1] = 0
										end
									end

								else

									graph_anal = trainee.analytics.where(:time_type => "month", 
											:year => starting.year, :month => m, :title => er["title"]).first

									graph_btns["#{er["title"]}"] = [0,0,0,0,0,0,0,0,0,0,0,0]

									if graph_anal
										graph_btns["#{er["title"]}"][m-1] = graph_anal.fbk
									else
										graph_btns["#{er["title"]}"][m-1] = 0
									end

								end

							end
						end
					end

					if (red + yellow + green) != 0

						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
					
						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
					end

					if (big_red + big_yellow + big_green) != 0
						big_avg = (big_red*1 + big_yellow*2 + big_green*3) / (big_red + big_yellow + big_green)

						big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-big_avg).abs }
					end

					big_graph["big_graph_datapoints"][m-1] = big_fbk
				end

				result << {:year => year, :month => m, :count => month_entries.count, :feedback => fbk}
			end

			overall_fbk = trainee.feedbacks.where(:type => "year", :year => year, :trainer => current_trainer.id.to_s).first

			if (real_big_colors[0] + real_big_colors[1] + real_big_colors[2]) != 0
				real_big_avg = (real_big_colors[0]*1 + real_big_colors[1]*2 + real_big_colors[2]*3) / 
					(real_big_colors[0] + real_big_colors[1] + real_big_colors[2])

				real_big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-real_big_avg).abs }
			end

			big_graph["fbk_color"] = real_big_fbk

			Time.zone = current_timezone

			final = {:result => result, :btns => btns, :graph_btns => graph_btns, :big_graph => big_graph, 
				:exc_btns => excluded_buttons}

			if overall_fbk
				final[:overall_fbk] = overall_fbk.as_json
			end

			render :json => final			
		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

	def home_day
		if current_trainer

			current_timezone = Time.zone
			Time.zone = "UTC"

			exc = []

			if params["except"]
				exc = params["except"]
			end

			result = []
			btns   = {}
			graph_btns = {}
			excluded_buttons = {}

			big_graph = {}

			big_graph["big_graph_datapoints"] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


			year  =  params["year"]

			month =  params["month"]

			day_count = Time.days_in_month(month.to_i, year.to_i)

			templates = current_trainer.templates.distinct(:id)

			trainee   = Trainee.where(:email => params["trainee"]["email"]).first
			
			shared = trainee.shares.where(:trainer_id => current_trainer.id.to_s).distinct(:template_id)
			
			templates << shared
			
			templates.flatten!
			
			templates.uniq!

			all_entries = Entry.where(:trainee_email => params["trainee"]["email"]).any_in(:template_id => templates)

			real_big_fbk = 0
			real_big_colors = [0.0, 0.0, 0.0]

			(1..day_count).each do |d|

				fbk = 0

				big_fbk = 0

				starting = Time.zone.parse("#{year}-#{month}-#{d}")

				ending   = Time.zone.parse("#{year}-#{month}-#{d}") + 1.day

				entries = all_entries.where(:created_at.gt => starting, :created_at.lt => ending)

				fbk_entries = entries.where(:feedback => true)

				if fbk_entries.count != 0

					red = 0.0
					green = 0.0
					yellow = 0.0

					big_red = 0.0
					big_green = 0.0
					big_yellow = 0.0
					
					fbk_entries.each do |e|
						e.entry_fields["responses"].each do |er|
							if er["active"] == "1"

								er["subs"].each do |sub|
									if sub["active"] == "1"
										if sub["feedback"] == "1"
											big_red += 1
											real_big_colors[0] += 1
										elsif sub["feedback"] == "2"
											big_yellow += 1
											real_big_colors[1] += 1
										elsif sub["feedback"] == "3"
											big_green += 1
											real_big_colors[2] += 1
										end
									end
								end

								if er["title"].in?(exc) == false
									if !btns.has_key?(er["title"])
										anal = trainee.analytics.where(:time_type => "month", :month => starting.month,
									 	 :year => starting.year, :title => er["title"]).first

										if anal
											btns["#{er["title"]}"] = anal.fbk
										end
									end

									er["subs"].each do |sub|
										if sub["active"] == "1"
											if sub["feedback"] == "1"
												red += 1
											elsif sub["feedback"] == "2"
												yellow += 1
											elsif sub["feedback"] == "3"
												green += 1
											end
										end
									end
								else

									if !btns.has_key?(er["title"])
										btns["#{er["title"]}"] = -1
									end

									if !excluded_buttons.has_key?(er["title"])
										exc_anal = trainee.analytics.where(:time_type => "month", 
											:month => starting.month, :year => starting.year, :title => er["title"]).first

										if exc_anal
											excluded_buttons["#{er["title"]}"] = exc_anal.fbk
										end
									end

								end

								if graph_btns.has_key?("#{er["title"]}")

									if graph_btns["#{er["title"]}"][d-1] == 0

										graph_anal = trainee.analytics.where(:time_type => "day", 
											:year => starting.year, :month => starting.month, :day => d, 
											:title => er["title"]).first

										if graph_anal
											graph_btns["#{er["title"]}"][d-1] = graph_anal.fbk
										else
											graph_btns["#{er["title"]}"][d-1] = 0
										end
									end

								else

									graph_anal = trainee.analytics.where(:time_type => "day", 
											:year => starting.year, :month => starting.month, :day => d, 
											:title => er["title"]).first

									graph_btns["#{er["title"]}"] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

									if graph_anal
										graph_btns["#{er["title"]}"][d-1] = graph_anal.fbk
									else
										graph_btns["#{er["title"]}"][d-1] = 0
									end

								end

							end
						end
					end

					if (red + yellow + green) != 0

						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
					
						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }

					end

					if (big_red + big_yellow + big_green) != 0
						big_avg = (big_red*1 + big_yellow*2 + big_green*3) / (big_red + big_yellow + big_green)

						big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-big_avg).abs }
					end

					big_graph["big_graph_datapoints"][d-1] = big_fbk
				end


				result << {:year => year, :month => month, :day => d, :count => entries.count, :feedback => fbk}

			end

			overall_fbk = trainee.feedbacks.where(:type => "month", :month => month, :year => year,
			 :trainer => current_trainer.id.to_s).first

			if (real_big_colors[0] + real_big_colors[1] + real_big_colors[2]) != 0
				real_big_avg = (real_big_colors[0]*1 + real_big_colors[1]*2 + real_big_colors[2]*3) / 
					(real_big_colors[0] + real_big_colors[1] + real_big_colors[2])

				real_big_fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-real_big_avg).abs }
			end

			big_graph["fbk_color"] = real_big_fbk
			
			Time.zone = current_timezone

			final = {:result => result, :btns => btns, :graph_btns => graph_btns, :big_graph => big_graph, 
				:exc_btns => excluded_buttons}

			if overall_fbk
				final[:overall_fbk] = overall_fbk.as_json
			end

			render :json => final

		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end 

	def list_day_entries
		if current_trainer

			current_timezone = Time.zone
			Time.zone = "UTC"

			exc = []

			if params["except"]
				exc = params["except"]
			end

			result = []
			btns   = {}

			starting = Time.zone.parse(params["date"])
			ending   = Time.zone.parse(params["date"]) + 1.day

			templates = current_trainer.templates.distinct(:id)

			trainee   = Trainee.where(:email => params["trainee"]["email"]).first
			
			shared = trainee.shares.where(:trainer_id => current_trainer.id.to_s).distinct(:template_id)
			
			templates << shared
			
			templates.flatten!
			
			templates.uniq!

			entries = Entry.where(:trainee_email => params["trainee"]["email"], :created_at.gt => starting,
			 :created_at.lt => ending).any_in(:template_id => templates).asc(:created_at)

			entries.each do |e|

				fbk = 0

				if e.feedback
					
					red = 0.0
					green = 0.0
					yellow = 0.0

					e.entry_fields["responses"].each do |er|
						if er["active"] == "1"

							if er["title"].in?(exc) == false
								if !btns.has_key?(er["title"])
									anal = trainee.analytics.where(:time_type => "day", :day => starting.day, :month => starting.month,
							 		 :year => starting.year, :title => er["title"]).first

									if anal
										btns["#{er["title"]}"] = anal.fbk
									end
								end

								er["subs"].each do |sub|
									if sub["active"] == "1"
										if sub["feedback"] == "1"
											red += 1
										elsif sub["feedback"] == "2"
											yellow += 1
										elsif sub["feedback"] == "3"
											green += 1
										end
									end
								end
							else

								if !btns.has_key?(er["title"])
									btns["#{er["title"]}"] = -1
								end

							end

						end
					end

					if (red + yellow + green) != 0

						avg = (red*1 + yellow*2 + green*3) / (red + yellow + green)
					
						fbk = [1.0, 2.0, 3.0].min_by{ |v| (v-avg).abs }
					end
				end

				result << {:entry => e.as_json.except("file_file_name", "file_content_type", "file_file_size", "file_updated_at",
					"file_fingerprint"), :file => {:med => e.file.url(:medium), :org => e.file.url}, 
					:template => e.template.name, :feedback => fbk}
			end

			overall_fbk = trainee.feedbacks.where(:type => "day", :day => starting.day, :month => starting.month,
			 :year => starting.year, :trainer => current_trainer.id.to_s).first
			
			Time.zone = current_timezone

			final = {:result => result, :btns => btns}

			if overall_fbk
				final[:overall_fbk] = overall_fbk.as_json
			end

			render :json => final

		else
			render :json => {:message => "User not found"}, :status => 404
		end
	end

    def template_params
		params.require(:template).except!(:loc).permit!	
    end

    def trainee_params
		params.require(:trainee).permit(:email)   	
    end

    def register_push_params
    	params.require(:push).permit(:push_token)
    end

    def feedback_params
    	params.require(:feedback).permit(:type, :year, :month, :day, :text)
    end

    	def login
		@trainer = Trainer.new
	end
end
