class ResetsController < ApplicationController
    
    require 'mandrill'
    skip_before_action :verify_authenticity_token
    #rescue_from Exception, :with => :throw_error
    #rescue_from Mongoid::Errors::Validations, :with => :throw_error
    
    def trainee_reset_password_token
            @trainee = Trainee.where(:email => params[:email]).first
            if @trainee
                    raw, enc = Devise.token_generator.generate(@trainee.class, :reset_password_token)
                    @trainee.reset_password_token   = enc[0..8]
                    @trainee.reset_password_sent_at = Time.now.utc
                    @trainee.save(validate: false)
                    
                    m = Mandrill::API.new
                    
                    message = {  
                      :to=>[  
                        {  
                          :email=> @trainee.email  
                        }  
                      ],  
                      :from_name=> "Bluewave",
                      :from_email => "osman.ehmad@gmail.com",
                      :text => "Password Reset Token: #{@trainee.reset_password_token}",
                      :html => "<p>Password Reset Token: #{@trainee.reset_password_token}</p>",
                      :subject => "Password Recovery",
                    }
                    
                    async = false
                    ip_pool = "Main Pool"
                    
                    result = m.messages.send message, async, ip_pool
                    
                    render :json => {:message => "email sent"}, :status => 200
            else
                    render :json => {:message => "bad email address"}, :status => 404
            end        
    end
            
    def trainer_reset_password_token
            @trainer = Trainer.where(:email => params[:email]).first
            if @trainer
                    raw, enc = Devise.token_generator.generate(@trainer.class, :reset_password_token)
                    @trainer.reset_password_token   = enc[0..8]
                    @trainer.reset_password_sent_at = Time.now.utc
                    @trainer.save(validate: false)
                    
                    m = Mandrill::API.new
                    
                    message = {  
                      :to=>[  
                        {  
                          :email=> @trainer.email  
                        }  
                      ],  
                      :from_name => "Bluewave",
                      :from_email => "osman.ehmad@gmail.com",
                      :text => "Password Reset Token: #{@trainer.reset_password_token}",
                      :html => "<p>Password Reset Token: #{@trainer.reset_password_token}</p>",
                      :subject => "Password Recovery",
                    }
                    
                    async = false
                    ip_pool = "Main Pool"
                    
                    result = m.messages.send message, async, ip_pool
                    
                    render :json => {:message => "email sent"}, :status => 200
            else
                    render :json => {:message => "bad email address"}, :status => 404
            end        
    end

    def trainee_change_password

      trainee = Trainee.where(:email => params[:email]).first

      if trainee.reset_password_token == params[:reset_token]

        trainee.update_attributes!(:password => params[:new_password])

        render :json => {:message => "Changed password"}, :status => 200

      end

    end
            
    def trainer_change_password
        
        trainer = Trainer.where(:email => params[:email]).first

      if trainer.reset_password_token == params[:reset_token]

        trainer.update_attributes!(:password => params[:new_password])

        render :json => {:message => "Changed password"}, :status => 200

      end

    end
    
    
end
