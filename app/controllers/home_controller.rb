class HomeController < ApplicationController
  before_filter :authenticate_user!

  def trainerSignIn
 	render layout: "vide" 
  end
  def index
	render new:"/home/users", layout: "pagecomplete"
  end

  def user
	render layout: "pagecomplete"
  end

  def trainer
	render layout: "pagecomplete"
  end
	
  def trainee
	render layout: "pagecomplete"
  end

  def option
	render layout: "pagecomplete"
  end

  def expandableRowTemplate
	render layout: "vide"
  end

 def listAllUsers    
    @users = []
    User.all.each do |t|
      @users << t
    end
    render :json => @users

  end

  def listAllTrainers
    @trainers=[]
    Trainer.all.each do |t|
         @trainers << t
    end
    render :json => @trainers
  end

  def listAllTrainees
	trainer=Trainer.where(:email => params["email"]).first
	@trainees = []
	@trainees=trainer.trainees
    render :json => @trainees
  end

  def listAllPackages

    if Package.all.length == 0
	package=Package.create
	package.title="Trial Adivisor"
	package.max_users=20
	package.price=0
	package.type=1
	package.update_attributes
    end

    @packages=[]
    Package.all.each do |t|
      @packages << t
    end
    render :json => @packages
  end

  def listAllDiscounts
    if Discount.all.length == 0
	discountprepaid=Discount.create
	discountprepaid.title="A 12 month pre-paid subscription"
	discountprepaid.percentage=20
	discountprepaid.type=1
	discountprepaid.update_attributes

	discountup=Discount.create
	discountup.title="Subscription package up"
	discountup.percentage=15
	discountup.type=2
	discountup.update_attributes
    end    
 
    @discounts=[]
    Discount.all.each do |t|
      @discounts << t
    end
    render :json => @discounts
  end

  def updateTrainer
	trainer=Trainer.where(:email=>params[:email]).first
	trainer.isActive=!trainer.isActive
	trainer.update_attributes 
	render :json => trainer
  end

  def updatePackage
	pack=Package.where(:type=>params[:type]).first
	if params[:price]!= nil
		pack.price= params[:price]

	elsif params[:max_users]!= nil
		pack.max_users= params[:max_users]
	elsif params[:title]!= nil
		pack.title= params[:title]
	end
	pack.statut= params[:statut]
	pack.update_attributes
  end

  def updateDiscount
	discount=Discount.where(:type=>params[:type]).first
	if params[:percentage]!= nil
		discount.percentage= params[:percentage]
	elsif params[:title]!= nil
		discount.title= params[:title]
	end
	discount.update_attributes
  end

  def package
	render layout: "pagecomplete"
  end

  def savePackage
	package=Package.create
	package.title=params[:package][:title]
	package.max_users=params[:package][:max_users]
	package.price=params[:package][:price]
	package.type=Package.all.length+1
	
	package.update_attributes
	redirect_to :action => 'option'
  end



end
