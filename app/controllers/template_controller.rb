class TemplateController < ApplicationController
	before_filter :token_authentication
	skip_before_action :verify_authenticity_token
	#rescue_from Exception, :with => :throw_error
	#rescue_from Mongoid::Errors::Validations, :with => :throw_error

	def show_all

		templates = []
		e_timestamp 		= Time.now

		if current_trainee	

			trnee_templates = current_trainee.templates_array - current_trainee.hidden_templates
			
			trnee_templates.each do |t|
				temp      =  Template.find(t)

				blocked = []

				temp.template_fields["items"].each_with_index do |i, index|

					blocked[index] = []

					if i["subs"]
						i["subs"].each_with_index do |l, sub_index|
							if l["variable_frequency"] != "Always" && l["variable_frequency"] != "Unlimited"

								t_frequency 		= l["variable_frequency"].split(" x ")[0]
								t_frequency_type 	= l["variable_frequency"].split(" x ")[1]

								cnt = 0

								if    t_frequency_type == "Day"
									all_entries = temp.entries.where(:trainee_email => current_trainee.email, :created_at.gt =>
									 e_timestamp.beginning_of_day, :created_at.lt => e_timestamp.end_of_day)
								elsif t_frequency_type == "Week"
									all_entries = temp.entries.where(:trainee_email => current_trainee.email, :created_at.gt =>
									 e_timestamp.beginning_of_week, :created_at.lt => e_timestamp.end_of_week)					
								elsif t_frequency_type == "Month"
									all_entries = temp.entries.where(:trainee_email => current_trainee.email, :created_at.gt =>
									 e_timestamp.beginning_of_month, :created_at.lt => e_timestamp.end_of_month)										
								end

								all_entries.each do |e|
									e.entry_fields["responses"].each do |r|
										r["subs"].each do |s|
											if s["title"] == l["title"] && s["response"] != nil && s["response"] != ""
												cnt += 1
											end
										end
									end
								end

								if cnt >= t_frequency.to_i
									blocked[index] << sub_index  
								end
							end
						end	
					end
					
				end

				templates << {:template => temp, :trainer => "#{temp.trainer.f_name} #{temp.trainer.l_name}",
				 :blocked => blocked}
			end

		elsif current_trainer

			current_trainer.templates.each do |t|
				templates << {:template => t, :trainer => "#{current_trainer.f_name} #{current_trainer.l_name}"}
			end
			# Add shared templates here
		end

		render :json => templates
	end

	def delete_template
		if current_trainer

			if params[:idz]
				params[:idz].each do |id|
					t = Template.find(id)

					if t
						t.delete
					end

					trnz = Trainee.any_of(:templates_array => id)

					trnz.each do |tr|
						all = tr.templates_array
						all.delete(id)
						tr.update_attributes!(:templates_array => all)
					end
				end	
			end

			render :json => {:message => "Successfully deleted templates"}, :status => 201
			
		end
	end

	def show
		if current_trainee || current_trainer
			render :json => {:templates => Template.where(:id => params["template"]["template_id"]).first}
		end
	end

	def show_entry
		if current_trainee || current_trainer

			e = Entry.where(:id => params["entry"]["entry_id"]).first

			if e
				render :json => {:entry => e.as_json.except("file_file_name", "file_content_type", "file_file_size", "file_updated_at",
						   "file_fingerprint"), :file => {:med => e.file.url(:medium), :org => e.file.url}}, :status => 200
			else
				render :json => {:message => "Entry not found"}, :status => 404
			end
		end
	end
end
